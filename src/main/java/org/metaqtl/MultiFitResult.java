/*  
 *  src/org/metaqtl/MultiFitResult.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

/**
 * This class represents a result of a multilinear regression
 * analysis.
 */
public class MultiFitResult {
	/**
	 * The p-value of the model.
	 */
	public double pvalue;
    /**
     * The parameter estimates.
     */
	public double[] theta;
	/**
	 * The variance-covariance matrix of the parameter
	 * estimates.
	 */
	public double[][] cov;
	/**
	 * The chi-square of the model.
	 */
	public double chi2;
	/**
	 * The residual standard deviations.
	 */
	public double[] rsd;
	/**
	 * The number of parameters
	 */
	public int m; 
	/**
	 * The number of observations without missing data.
	 */
	public int n;
	/**
	 * The number of degrees of freedom.
	 */
	public int dof;
	/**
	* Creates a new instance of <code>MultiFitResult</code>
	* with the given number of observations <code>N</code>
	* and parameters <code>M</code>.
	* @param N the number of observations.
	* @param M the number of parameters.
	*/
	public MultiFitResult(int N, int M) {
		theta      	  = new double[N];
		rsd = new double[N];
		cov    	  = new double[N][N];
		chi2      = 0.;
		dof       = 0;
		pvalue    = 1.;		
	}
	
	public String toString(){
		String ret="";
		ret+= "*** Regression Result\n";
		ret+= "chi2      = "+chi2+"\n";
		ret+= "dof        = "+dof+"\n";
		ret+= "pvalue  = "+pvalue+"\n";
		return ret;
	}
}
