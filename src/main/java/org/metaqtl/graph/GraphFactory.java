/*  
 *  src/org/metaqtl/graph/GraphFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.graph;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * 
 */
public class GraphFactory {
	
	public static int WIDTH  = 5000;
	
	public static int HEIGHT = 500;
	
	public static int BOX_WIDTH  = 4000;
	
	public static int BOX_HEIGHT = 300;
	
	public static BufferedImage getImage(ChromGraph[] chroms) {
		int i,W,H;
		double sx,sy;
		
		if (chroms == null) return null;
		
		MetaGraph mg = new MetaGraph();
		
		// Get the default graphics context
		BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		Graphics2D graph = bi.createGraphics();
		
		mg.init(graph, chroms);
		
		HEIGHT = (int)mg.getHeight();
		HEIGHT += (int)HEIGHT*0.1;
		WIDTH  = (int)mg.getWidth();
		WIDTH += (int)WIDTH*0.1;
		
		if (WIDTH > 3500) {
			W  = 3500;
			sx = (double)W/(double)WIDTH;
			H  = W*HEIGHT/WIDTH;
			sy = (double)H/(double)HEIGHT;
		} else {
			W = WIDTH;
			H = HEIGHT;
			sx=sy=0.0;
		}
		
		
		bi = new BufferedImage(W, H, BufferedImage.TYPE_INT_RGB);
		graph = bi.createGraphics();
		graph.setBackground(MetaGraphPar.BACKGROUND_COLOR);
		graph.clearRect(0,0,W,H);
		
		if (sx > 0.0 && sy > 0.0) {
			AffineTransform s = new AffineTransform();
			s.scale(sx,sy);
			graph.transform(s);
		}
		mg.draw(graph, WIDTH*0.05, HEIGHT*0.05);
		graph.dispose();
		
		bi.flush();
	  
		return bi;
	}
}