/*  
 *  src/org/metaqtl/graph/QtlLayerBuffer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.graph;

import java.awt.Graphics2D;
import java.util.ArrayList;

/**
 * 
 */
public class QtlLayerBuffer {
	
	private ArrayList qtlShapeList;
	
	/**
	 * Try to add a qtl unit to the configuration.
	 * If the qtl has been adde then the method returns true
	 * otherwise it returns false.
	 * @param graph
	 * @param font
	 * @param unit
	 * @return
	 */
	public boolean addQtl(Graphics2D graph, QtlUnit unit) {
		double d;
		boolean add=false;
		
		QtlShape shape  = new QtlShape();
		shape.buildShape(graph, unit);
		
		if (qtlShapeList!=null) {
			
			QtlShape prev    = (QtlShape) qtlShapeList.get(qtlShapeList.size()-1);
			
			d = QtlShape.getYDistance(shape,prev);
			
			if (d > MetaGraphPar.QTL_VSPACE) {
				add = true;
			}
			
		} else {
			
			add=true;
			qtlShapeList = new ArrayList();
		}
		
		if (add) {
			
			qtlShapeList.add(shape);
		}
		
		return add;
	}
	/**
	 * Retruns true if more Qtl can be added to the configuration,
	 * false otherwise.
	 * @return
	 */
	public boolean canAddMoreQtl(double ymax) {
		if (qtlShapeList != null) {
			// Get the latest added shape 
			QtlShape prev    = (QtlShape) qtlShapeList.get(qtlShapeList.size()-1);
			return (prev.getYMax() < ymax);
		}
		return true;
	}
	/**
	 * Returns a QtlLayer which represent the contents of
	 * the buffer.
	 * 
	 * @return
	 */
	public QtlLayer getLayer(double x, double y) {
		QtlLayer layer = null;
		
		if (qtlShapeList != null && qtlShapeList.size() > 0) {
			layer  = new QtlLayer(x, y);
			layer.qtlShapes = new QtlShape[qtlShapeList.size()];
			qtlShapeList.toArray(layer.qtlShapes);
			layer.attach(this);
		}
		return layer;
		
	}
}
