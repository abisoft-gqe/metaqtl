/*  
 *  src/org/metaqtl/graph/QtlLayer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.graph;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

public class QtlLayer extends Layer {
	/**
	 * The default width of the CI of the Qtl
	 */
	public static double CI_WIDTH   	       = 5.0;
	/**
	 * The default cex parameter for the tick height
	 * of the Qtl
	 */
	public static double TICK_CEX_HEIGHT 	   = 0.05;
	/**
	 * The default cex parameter for the tick width
	 * of the Qtl
	 */
	public static double TICK_CEX_WIDTH        = 1.5;
	/**
	 * The default width of the Text attribute of the
	 * Qtl
	 */
	public static double TEXT_WIDTH            = 10.0;
	/**
	 * The Qtl shapes.
	 */
	public QtlShape[] qtlShapes 			   = null;
	/**
	 * The Qtl configuration attached to this layer.
	 */
	private QtlLayerBuffer           qtlBuffer = null;
	/**
	 * @param x
	 * @param y
	 * @param size
	 * @param type
	 */
	public QtlLayer(double x, double y) {
		super(x, y);
	}
	
	public void draw(Graphics2D graph) {
		if (qtlShapes != null) {
			Color           c     = graph.getColor();
			AffineTransform t     = graph.getTransform();
			AffineTransform trans = new AffineTransform();
			trans.translate(x,y);
			graph.transform(trans);
			for(int i=0;i<qtlShapes.length;i++) {
				qtlShapes[i].draw(graph);
			}
			graph.setTransform(t);
			graph.setColor(c);
		}
	}
	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.graph.Layer#buildLayer()
	 */
	public void build(Graphics2D graph) {
		width=0.0;
		height=0.0;
		if (qtlShapes != null) {
			for(int i=0;i<qtlShapes.length;i++) {
				if (qtlShapes[i].getWidth() > width) width = qtlShapes[i].getWidth();
				if (i==0) height -= qtlShapes[i].getYMin();
				if (i==qtlShapes.length-1) height += qtlShapes[i].getYMax();
			}
		}	
	}
	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.graph.Layer#attach(java.lang.Object)
	 */
	public void attach(Object object) {
		if (object instanceof QtlLayerBuffer) {
			qtlBuffer = (QtlLayerBuffer) object;
		}
	}

	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.graph.Layer#getYMin()
	 */
	public double getYMin() {
		return y+qtlShapes[0].getYMin();
	}

	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.graph.Layer#getYMax()
	 */
	public double getYMax() {
		return y+qtlShapes[qtlShapes.length-1].getYMax();
	}
}