/*  
 *  src/org/metaqtl/graph/ChromGraph.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.graph;

import org.metaqtl.Chromosome;
import org.metaqtl.QtlPartition;

/**
 * A <code>ChromGraph</code> allows to link together
 * a <code>Chromosome</code> and a <code>QtlPartition</code>
 * in order to be depicted into a graphic.
 */
public final class ChromGraph {
	/**
	 * The chromosome to draw
	 */
	public Chromosome   chromosome;
	/**
	 * The Qtl partition 
	 */
	public QtlPartition qtlPart;
}
