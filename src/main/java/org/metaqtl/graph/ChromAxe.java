/*  
 *  src/org/metaqtl/graph/ChromAxe.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.graph;

/**
 * 
 */
public class ChromAxe {
	/**
	 * The original length of the 
	 * chromosome.
	 */
	private double length = 0.0;
	/**
	 * The scale parameter between
	 * the original axe on the chromosome
	 * and this axe.
	 */
	private double scale  = 0.0;
	
	private double yMin   = 0.0;
	
	private double yMax   = 0.0;
	
	private double x = 0.0;
	
	public ChromAxe(double yMin, double yMax, double length) {
		this.yMin = yMin;
		this.yMax = yMax;
		this.length = length;
		this.scale  = (yMax-yMin)/length;
	}
	
	/**
	 * @return Returns the yMax.
	 */
	public double getYMax() {
		return yMax;
	}
	/**
	 * @param max The yMax to set.
	 */
	public void setYMax(double max) {
		yMax = max;
	}
	/**
	 * @return Returns the yMin.
	 */
	public double getYMin() {
		return yMin;
	}
	/**
	 * @param min The yMin to set.
	 */
	public void setYMin(double min) {
		yMin = min;
	}
	/**
	 * @return
	 */
	public double getHeight() {
		return yMax-yMin;
	}
	/**
	 * Do a affine transformation of the given
	 * position <code>d</code> which is assumed
	 * to be in [0,dmax] in order to have its
	 * representation in the chromosome axe.
	 * 
	 * @param d
	 * @return
	 */
	public double transformY(double d) {
		return yMin + scale*d;
	}
	
	public double scaleY(double d) {
		return scale*d;
	}
	/**
	 * @param d
	 * @return
	 */
	public double scaleHeight(double h) {
		return h*scale;
	}
	/**
	 * Scale the axe in y.
	 * @param sy
	 */
	public void scale(double sy) {
		yMin *= sy;
		yMax *= sy;
		scale  = (yMax-yMin)/length;
	}

	/**
	 * @return Returns the x.
	 */
	public double getX() {
		return x;
	}
	/**
	 * @param x The x to set.
	 */
	public void setX(double x) {
		this.x = x;
	}
}
