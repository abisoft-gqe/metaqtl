/*  
 *  src/org/metaqtl/graph/PaletteColor.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.graph;

import java.awt.Color;

/**
 * 
 */
public final class PaletteColor {
	
	
	public static final Color base[] = {
											Color.yellow,
											Color.green,
											Color.blue,
											Color.orange,
											Color.red,
											Color.lightGray,
											Color.cyan,
											Color.magenta
										  };
	
	/**
	 * Returns a palete of colors which should be distinct.
	 * 
	 * @param ncolor
	 * @return
	 */
	public static Color[] getPalette(int ncolor) {
		int nb = base.length;
		int nd = ncolor/nb;
		
		Color[] colors = new Color[ncolor];
		
		for(int i=0;i<Math.min(nb,ncolor);i++)
			colors[i]=base[i];
		
		for(int i=0,j=nb;i<nd;i++) {
			for(int k=0;k<ncolor-nd*nb;k++,j++) {
				if (i%2==0)
					colors[j]=base[k].darker();
				else
					colors[j]=base[k].brighter();
			}
		}
		
		return colors;
	}

	/**
	 * @param proba_bin
	 * @param yellow
	 * @param red
	 * @return
	 */
	public static Color[] getPaletteGradient(int nc, Color from, Color to) {
		int r,g,b;
		Color[] palete = new Color[nc];
		
		palete[0]    = from;
		palete[nc-1] = to;
		
		r = to.getRed()-from.getRed();
		g = to.getGreen()-from.getGreen();
		b = to.getBlue()-from.getBlue();
		
		for(int i=1;i<nc-1;i++) {
			palete[i] = new Color(from.getRed() + i*r/(nc-1), from.getGreen() + i*g/(nc-1), from.getBlue() + i*b/(nc-1));
		}
		
		return palete;
	}

}
