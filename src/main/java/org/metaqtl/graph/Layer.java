/*  
 *  src/org/metaqtl/graph/Layer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.graph;

import java.awt.Graphics2D;

public abstract class Layer extends AnchoredElement {
	/**
	 * The width of the layer.
	 */
	protected double width = 0.0;
	/**
	 * The height of the layer.
	 */
	protected double height = 0.0;
	/**
	 * Creates a layer without any width and heigth
	 * constraints which upper left corner will be
	 * located at the point (x,y).
	 * @param x the abscisse of the upper left corner of the layer.
	 * @param y the ordinate of the upper left corner of the layer.
	 */
	public Layer(double x, double y) {
		this.x = x;
		this.y = y;
	}
	/**
	 * Attach the given <code>object</code> to the layer.
	 * @param object the object to attach to the layer.
	 */
	public abstract void attach(Object object);
	/**
	 * Build the layer.
	 */
	public abstract void build(Graphics2D graph);
	/**
	 * Draws the layer on the given <code>graph</code> context.
	 * @param graph
	 */
	public abstract void draw(Graphics2D graph);
	/**
	 * @return Returns the height.
	 */
	public double getHeight(){
		return height;
	}
	/**
	 * @return Returns the width.
	 */
	public double getWidth() {
		return width;
	}
	
	public abstract double getYMin();
	
	public abstract double getYMax();
	
}