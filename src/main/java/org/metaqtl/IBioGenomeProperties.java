/*  
 *  src/org/metaqtl/IBioGenomeProperties.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

/**
 * This interface defines both the keys and the 
 * default values of properties which
 * can be handled by a org.thalia.bio.entity.IBioGenome.
 */
public interface IBioGenomeProperties {
	/**
	 * The cross size key.
	 */
	public static final String CROSS_SIZE 				= "cross.size";
	/**
	 * The cross type key.
	 */
	public static final String CROSS_TYPE 				= "cross.type";
	/**
	 * The mapping unit key.
	 */
	public static final String MAPPING_UNIT 			= "mapping.unit";
	/**
	 * The code for Morgan unit.
	 */
	public static final String MAPPING_UNIT_M 			= "M";
	/**
	 * The code for centi-Morgan unit.
	 */
	public static final String MAPPING_UNIT_CM 			= "cM";
	/**
	 * The key for mapping function.
	 */
	public static final String MAPPING_FUNCTION 		= "mapping.function";
	/**
	 * The code for Haldane mapping function.
	 */
	public static final String MAPPING_FUNCTION_HALDANE = "haldane";
	/**
	 * The code for Kosambi mapping function.
	 */
	public static final String MAPPING_FUNCTION_KOSAMBI = "kosambi";
	/**
	 * The key for mapping expansion status.
	 */
	public static final String MAPPING_EXPANSION 		= "mapping.expansion";
	/**
	 * The key for mapping cross.
	 */
	public static final String MAPPING_CROSS 			= "mapping.cross";	
}
