/*  
 *  src/org/metaqtl/IMetaAlgorithm.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

import java.io.OutputStream;
import java.io.Writer;

/**
 * This interface defines some methods which are common
 * to data analysis algorithms.
 */
public interface IMetaAlgorithm {
	/**
	 * Run the algorithm.
	 */
	public void run();
	/**
	 * Returns the amount of the work to do.
	 * @return
	 */
	public int getWorkAmount();
	/**
	 * Returns the amount of work which have already
	 * been done.
	 * @return
	 */
	public int getWorkProgress();
	/**
	 * Give a <code>Writer</code> to the algorithm
	 * in order to write out the log info. The user
	 * is responsible for closing the stream.
	 * @param writer
	 */
	public void setLogger(Writer writer);
	/**
	 * Give a <code>OutputStream</code> to the algorithm
	 * in order to write out the log info. The user
	 * is responsible for closing the stream.
	 * @param stream
	 */
	public void setLogger(OutputStream stream);
}
