/*  
 *  src/org/metaqtl/main/QTLProj.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.main;

import java.io.IOException;

import org.metaqtl.Chromosome;
import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.adapter.ChromosomeAdapter;
import org.metaqtl.algo.QtlProjAlgorithm;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioLocus;

/**
 * 
 * 
 */
public class QTLProj extends MetaMain {

	private static final String VERSION = IMetaQtlConstants.VERSION; 
	
	private static final String syntax  =  " Syntaxe: QTLProj "+
											"[{-m, --map}]] "+
											"[{-q, --qtl}]] "+
											"[{-o, --out}]] "+
											"[{-r, --ratio}]] "+
											"[{-p, --pval}]] "+
											"[{--rmrk}]] "+
											"[{--rqtl}]] "+
											MetaMain.generalUsage();
	
	public void printUsage() {
        System.err.println(syntax);
    }
	
	public void printHelp() {
		
		System.out.println("QTLProj, "+VERSION);
		System.out.println("Copyright (C) 2005  Jean-Baptiste Veyrieras (INRA)");
		System.out.println("QTLProj comes with ABSOLUTELY NO WARRANTY.  ");
		System.out.println("This is free software, and you are welcome  ");
		System.out.println("to redistribute it under certain conditions.");
		System.out.println();
		System.out.println(syntax);
		System.out.println();
        MetaMain.generalHelp();
        System.out.println();
        System.out.println("-m, --map : the location of the map file");
        System.out.println("-q, --qtl : the location of the qtl map file or directory");
        System.out.println("-o, --out : the output file stem");
        System.out.println("-r, --ratio : the minimal ratio");
        System.out.println("-p, --pval : the minimal pvalue");
        System.out.println("--rmrk : remove a given list of marker");
        System.out.println("--rqtl : remove a given list of QTL");
        
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		QTLProj program = new QTLProj();
		
		program.initCmdLineParser();
		
		CmdLineParser parser = program.parser;
		
		CmdLineParser.Option amapFile  = parser.addStringOption('m', "map");
        CmdLineParser.Option aqtlDir   = parser.addStringOption('q', "qtl");
        CmdLineParser.Option aoutFile  = parser.addStringOption('o', "out");
        CmdLineParser.Option aratio  = parser.addDoubleOption('r', "ratio");
        CmdLineParser.Option apval  = parser.addDoubleOption('p', "pval");
        CmdLineParser.Option arMrk  = parser.addStringOption("rmrk");
        CmdLineParser.Option arQtl  = parser.addStringOption("rqtl");
        
        
        program.parseCmdLine(args);
        
        // Mandatory
        String mapFile 	 =  (String)parser.getOptionValue(amapFile);
        String qtlDir  	 =  (String)parser.getOptionValue(aqtlDir);
        String outFile	 =  (String)parser.getOptionValue(aoutFile);
        Double ratio	 =  (Double)parser.getOptionValue(aratio, new Double(0.5));
        Double pval 	 =  (Double)parser.getOptionValue(apval, new Double(0.01));
        String rMrkFile	 =  (String)parser.getOptionValue(arMrk);
        String rQtlFile	 =  (String)parser.getOptionValue(arQtl);
                
        if (mapFile== null) {
        	System.err.println("No map location defined : EXIT");
        	System.exit(2);
		}
        if (qtlDir== null) {
        	System.err.println("No qtl location defined : EXIT");
        	System.exit(2);
		}
        if (outFile==null) {
        	System.err.println("No output location defined : EXIT");
        	System.exit(2);
		}
    	
        IBioGenome map      = null;
        IBioGenome[] qtls   = null;
        
        try {
			map  	= getMap(mapFile);
			qtls 	= getMaps(qtlDir);
			removeLocus(qtls, rMrkFile);
			removeLocus(qtls, rQtlFile);
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			System.exit(3);
			
		}
		
        if (map == null) {
        	
        	System.err.println("Unable to load the map from " + mapFile);
			System.exit(4);
        	
        }
        
        if (qtls == null) {
        	
        	System.err.println("Unable to load the QTL from " + qtlDir);
			System.exit(5);
        	
        }
		
		QtlProjAlgorithm algo = new QtlProjAlgorithm(qtls, map, null);
		
		if (!program.verbose.booleanValue())
        	algo.disableLogger();
		algo.setRatio(ratio.doubleValue());
		algo.setPvalue(pval.doubleValue());
		
		algo.run();
		
		Chromosome[] result = algo.getResult();
		
		IBioGenome pmap = ChromosomeAdapter.toIBioGenome(result, 0);
		IBioGenome pqtl = ChromosomeAdapter.toIBioGenome(result, 2);
		
		try {
			
			writeMap(pmap, outFile+"_map.xml");
			writeMap(pqtl, outFile+"_qtl.xml");
			
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			System.exit(6);
			
		}
		
		System.exit(0);
		
    }
}
