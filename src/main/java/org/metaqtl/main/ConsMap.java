/*  
 *  src/org/metaqtl/main/ConsMap.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.main;

import java.io.FileOutputStream;
import java.io.IOException;

import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.MetaDico;
import org.metaqtl.MetaMap;
import org.metaqtl.adapter.MetaMapAdapter;
import org.metaqtl.algo.MetaMapAlgorithm;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioLocus;
import org.metaqtl.factory.MetaMapSummaryFactory;


public class ConsMap extends MetaMain {
	
	private static final String VERSION = IMetaQtlConstants.VERSION; 
	
	private static final String syntax  =  "Syntaxe: ConsMap "+
											"[{-m, --mapdir}]] "+
											"[{-r, --refmap}]] "+
											"[{-t, --mrkth}]] "+
											"[{-o, --outstem}]] "+
											"[{-d, --dubfile}]] "+
											"[{--mrkdico}]] "+
											MetaMain.generalUsage();
	
	public void printUsage() {
        System.err.println(syntax);
    }
	
	public void printHelp() {
		
		System.out.println("ConsMap, "+VERSION);
		System.out.println("Copyright (C) 2005  Jean-Baptiste Veyrieras (INRA)");
		System.out.println("ConsMap comes with ABSOLUTELY NO WARRANTY.  ");
		System.out.println("This is free software, and you are welcome  ");
		System.out.println("to redistribute it under certain conditions.");
		System.out.println();
		System.out.println(syntax);
		System.out.println();
        MetaMain.generalHelp();
        System.out.println();
        System.out.println("-m, --mapdir  : the location of the directory of the map files");
        System.out.println("-r, --refmap  : the location of the reference map (optional)");
        System.out.println("-t, --mrkth   : threshold on the number of times a marker is observed");
        System.out.println("-o, --outstem : the stem name for output files");
        System.out.println("-d, --dubfile : the dubious markers to remove. (optional)");
        System.out.println("--mrkdico     : the marker dictionary (optional)");
        
	}
	

	public static void main(String[] args) {
		
		ConsMap program = new ConsMap();
		
		program.initCmdLineParser();
		
		CmdLineParser parser = program.parser;
		
        CmdLineParser.Option amapDir  = parser.addStringOption('m', "mapdir");
        CmdLineParser.Option arefMap  = parser.addStringOption('r', "refmap");
        CmdLineParser.Option amrkThresh  = parser.addIntegerOption('t', "mrkthresh");
        CmdLineParser.Option aoutStem = parser.addStringOption('o', "outstem");
        CmdLineParser.Option adubFile = parser.addStringOption('d', "dubfile");
        CmdLineParser.Option amrkDicoFile = parser.addStringOption("mrkdico");
        
        program.parseCmdLine(args);
        
        // Mandatory
        String mapDir  	 =  (String)parser.getOptionValue(amapDir);
        String refMap  	 =  (String)parser.getOptionValue(arefMap);
        Integer mrkThresh = (Integer)parser.getOptionValue(amrkThresh, new Integer(1));
        String outStem 	 =  (String)parser.getOptionValue(aoutStem);
        // Optional
        String dubFile 	 =  (String)parser.getOptionValue(adubFile, null);
        String mrkDicoFile =  (String)parser.getOptionValue(amrkDicoFile, null);
        
        if (mapDir== null) {
        	System.err.println("No map directory defined : EXIT");
        	System.exit(2);
		}
        if (outStem== null) {
        	System.err.println("No output stem defined : EXIT");
        	System.exit(2);
		}
        
        IBioGenome[] maps   = null;
        IBioGenome skeleton = null;
        IBioLocus[] dubious = null;
        MetaDico mrkDico = null;
        
		try {
			maps     = getMaps(mapDir);
			skeleton = getMap(refMap);
	        dubious  = getDubious(dubFile);
	        mrkDico  = getDico(mrkDicoFile);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(3);
		}
		
        if (maps == null) {
        	
        	System.err.println("Unable to read map files from " + mapDir);
        	System.exit(4);
        	
        }
		
        MetaMapAlgorithm algorithm = new MetaMapAlgorithm(maps, skeleton, dubious, mrkDico);
        algorithm.setMrkThreshold(mrkThresh.intValue());
        
        if (!program.verbose.booleanValue())
        	algorithm.disableLogger();
		
		algorithm.run();
		
		MetaMap metaMap = algorithm.getResult();
		
		/*
		 * Then convert the metaMap into a GeneticMap 
		 */
		IBioGenome consMap = MetaMapAdapter.toIBioGenome(metaMap);
		/*
		* Finally write out the genetic map...
		*/
		try {
			
			writeMap(consMap, outStem+"_map.xml");
			// Write out info on the model fit. 
			FileOutputStream stream = new FileOutputStream(outStem+"_fit.txt");
			MetaMapSummaryFactory.write(metaMap, stream);
			stream.close();
			
		} catch (IOException e) {
			
			System.err.println("Unable to save result");
        	System.exit(5);
			
		}
		
		System.exit(0);
		
	}
}
