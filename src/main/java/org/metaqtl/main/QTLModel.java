/*  
 *  src/org/metaqtl/main/QTLModel.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.main;

import java.io.IOException;

import org.metaqtl.Chromosome;
import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.MetaQtlAnalysis;
import org.metaqtl.MetaQtlModel;
import org.metaqtl.Qtl;
import org.metaqtl.adapter.ChromosomeAdapter;
import org.metaqtl.bio.IBioGenome;

/**
 * 
 */
public class QTLModel extends MetaMain {
	
	private static final String VERSION = IMetaQtlConstants.VERSION; 
	
	private static final String syntax  =  "Syntaxe: QTLModel "+
											"[{-m, --map}]] "+
											"[{-r, --clust}]] "+
											"[{-b, --best}]] "+
											"[{-o, --outfile}]] "+
											MetaMain.generalUsage();
	
	public void printUsage() {
        System.err.println(syntax);
    }
	
	public void printHelp() {
		
		System.out.println("QTLModel"+VERSION+", Copyright (C) 2005  Jean-Baptiste Veyrieras (INRA)");
		System.out.println("QTLModel comes with ABSOLUTELY NO WARRANTY.");
		System.out.println("This is free software, and you are welcome to redistribute it");
		System.out.println("under certain conditions;");
		System.out.println();
		System.out.println(syntax);
        System.out.println();
        MetaMain.generalHelp();
        System.out.println();
        System.out.println("-m, --map    : the file location of the map");
        System.out.println("-r, --clust  : the file location of the clustering result");
        System.out.println("-b, --best   : the file location of the best clustering");
        System.out.println("-o, --outfile: the output file");
        
	}
	
	public static void main(String[] args) {
		
		QTLModel program = new QTLModel();
		
		program.initCmdLineParser();
		
		CmdLineParser parser = program.parser;
		
        CmdLineParser.Option amapFile  = parser.addStringOption('m', "map");
        CmdLineParser.Option aresFile  = parser.addStringOption('r', "clust");
        CmdLineParser.Option acluFile  = parser.addStringOption('b', "best");
        CmdLineParser.Option aoutFile  = parser.addStringOption('o', "outfile");
        
        program.parseCmdLine(args);
        
        // Mandatory
        String mapFile  	 =  (String)parser.getOptionValue(amapFile);
        String resFile  	 =  (String)parser.getOptionValue(aresFile);
        String cluFile  	 =  (String)parser.getOptionValue(acluFile);
        String outFile  	 =  (String)parser.getOptionValue(aoutFile);
        
        if (mapFile == null) {
        	System.err.println("[ ERROR ] No map file defined");
        	System.exit(2);
		}
        if (resFile == null) {
        	System.err.println("[ ERROR ] No result file defined");
        	System.exit(2);
        }
        if (cluFile == null) {
        	System.err.println("[ ERROR ] No best model defined");
        	System.exit(2);
        }
        if (outFile == null) {
        	System.err.println("[ ERROR ] No output stem file defined");
        	System.exit(2);
        }
        
        IBioGenome map			= null;
        MetaQtlAnalysis result	= null;
		MetaQtlModel cluster	= null;
        
        try {
			
        	map  	= getMap(mapFile);
        	result  = getResult(resFile);
        	cluster = getCluster(cluFile);
			
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			System.exit(3);
			
		}
		if (map == null) {
			System.err.println("[ ERROR ]  Unable to load map from " + mapFile);
			System.exit(4);
		}
		if (result == null) {
			System.err.println("[ ERROR ]  Unable to load result from " + resFile);
			System.exit(4);
		}
		if (cluster == null) {
			System.err.println("[ ERROR ]  Unable to load best model from " + cluFile);
			System.exit(4);
		}
		
		Chromosome[] chromosomes = ChromosomeAdapter.toChromosomes(map);
		for(int i=0;i<chromosomes.length;i++) {
			Chromosome chrom = chromosomes[i];
			String[] traits  = cluster.getTraitNames(chrom.getName());
			if (traits != null)
			{
				for(int j=0;j<traits.length;j++) {
					int kclust  = cluster.getModel(chrom.getName(), traits[j]);
					Qtl[] mqtls = result.getMetaQtl(chrom.getName(), traits[j], kclust);
					chrom.attachQtl(mqtls);
				}
			}
		}
		// Get the map
		map = ChromosomeAdapter.toIBioGenome(chromosomes, 0);
		
		try {
			
			writeMap(map, outFile);
			
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			System.exit(5);
			
		}
		
		System.exit(0);
	}
	
	
}
