/*  
 *  src/org/metaqtl/main/A2Xml.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.main;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioOntology;
import org.metaqtl.bio.entity.factory.BioEntityFactory;
import org.metaqtl.bio.entity.factory.MapChartFactory;
import org.metaqtl.bio.entity.factory.MapMakerFactory;
import org.metaqtl.bio.entity.factory.MapTabFactory;
import org.metaqtl.bio.entity.factory.OntologyTabFactory;
import org.metaqtl.bio.entity.factory.XmlGeneticMapFactory;
import org.metaqtl.bio.entity.factory.XmlOntologyFactory;

/**
 * 
 */
public class A2Xml extends MetaMain {
	
	private static final String VERSION = IMetaQtlConstants.VERSION; 
	
	private static final String syntax  =  "Syntaxe: A2Xml "  +
											"[{-x, --xmlfile}]] " +
											"[{-t, --type}]] "    +
											"[{-f, --format}]] "  +
											"[{-i, --ifile}]] " +
											MetaMain.generalUsage();
	
	public void printUsage() {
        System.err.println(syntax);
    }
	
	public void printHelp() {
		
		MetaMain.printLicense("A2Xml", VERSION);
		System.out.println();
		System.out.println(syntax);
        System.out.println();
        MetaMain.generalHelp();
        System.out.println();
        System.out.println("-x, --xmlfile  : the xml file location.");
        System.out.println("-t, --type     : the input file type among this list");
        System.out.println("                   - map  : a marker and/or QTL map");
        System.out.println("                   - onto : an ontology");
        System.out.println("-f, --format   : the input ascii format among this list");
        System.out.println("                   if -t map : ");
        System.out.println("                            - tab   : Tabulated format");
        System.out.println("                            - mch   : MapChart  format");
        System.out.println("                            - mmk   : MapMaker  format");
        System.out.println("-i, --ifile    : the input file");
        
	}
	
	public static void main(String[] args) {
		
		Xml2A program = new Xml2A();
		
		program.initCmdLineParser();
		
		CmdLineParser parser = program.parser;
		
        CmdLineParser.Option axmlFile  = parser.addStringOption('x', "xmlfile");
        CmdLineParser.Option atype     = parser.addStringOption('t', "type");
        CmdLineParser.Option aformat   = parser.addStringOption('f', "format");
        CmdLineParser.Option ainFile   = parser.addStringOption('i', "ifile");
        
        program.parseCmdLine(args);
        
        // Mandatory
        String xmlFile  	 =  (String)parser.getOptionValue(axmlFile);
        String type  		 =  (String)parser.getOptionValue(atype);
        String format	  	 =  (String)parser.getOptionValue(aformat);
        String inFile    	 =  (String)parser.getOptionValue(ainFile);
        
        if (xmlFile == null) {
        	System.err.println("No xml file defined");
        	System.exit(2);
		}
        if (type == null) {
        	System.err.println("No xml type defined");
        	System.exit(2);
        }        
        if (inFile == null) {
        	System.err.println("No output file defined");
        	System.exit(2);
        }
        
        try {
            if (type.equals("map")) {
            	if (format == null) {
                	System.err.println("No output ascii format defined");
                	System.exit(2);
                }
            	IBioGenome map=null;
            	BioEntityFactory factory=null;
            	if (format.equals("tab")) factory=new MapTabFactory();
            	else if (format.equals("mch")) factory=new MapChartFactory();
            	else if (format.equals("mmk")) factory=new MapMakerFactory();
            	FileReader reader  = new FileReader(inFile);
            	map = (IBioGenome) factory.load(reader);
            	reader.close();
            	factory = new XmlGeneticMapFactory();
            	FileWriter writer = new FileWriter(xmlFile);
            	factory.unload(map, writer);
            	writer.close();
            } else if (type.equals("onto")) {
            	IBioOntology ontology = null;
            	BioEntityFactory factory=new OntologyTabFactory();
            	FileReader reader = new FileReader(inFile);
            	ontology = (IBioOntology) factory.load(reader);
            	reader.close();
            	FileWriter writer = new FileWriter(xmlFile);
            	factory = new XmlOntologyFactory();
            	factory.unload(ontology, writer);
            	writer.close();
            }
        } catch (IOException e) {
        	System.err.println(e.getMessage());
        	System.exit(3);
		}
        
        System.exit(0);
        	
	}

	
}
