/*  
 *  src/org/metaqtl/main/MapView.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.main;

import java.io.File;
import java.io.IOException;

import org.metaqtl.Chromosome;
import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.MetaQtlAnalysis;
import org.metaqtl.QtlPartition;
import org.metaqtl.adapter.ChromosomeAdapter;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioOntology;
import org.metaqtl.graph.ChromGraph;

/**
 * 
 */
public class MapView extends MetaMain {
	
	private static final String VERSION = IMetaQtlConstants.VERSION; 
	
	private static final String syntax  =   "Syntaxe: MapView "+
											"[{-m, --map}]] "+
											"[{-c, --chrom}]] "+
											"[{-q, --qtl}]] "+
											"[{--qmode}]] "+
											"[{-o, --outstem}]] "+
											"[{-t, --tonto}]] "+
											"[{-p, --par}]" +
											"[{--tree}]" +
											"[{--img}]" +										
											MetaMain.generalUsage();
	
	public void printUsage() {
        System.err.println(syntax);
    }
	
	public void printHelp() {
		MetaMain.printLicense("MapView", VERSION);
		System.out.println();
		System.out.println(syntax);
        System.out.println();
        MetaMain.generalHelp();
        System.out.println();
        System.out.println("-m, --map     : the file location of the map");
        System.out.println("-c, --chrom   : the chromosome(s) to draw");
        System.out.println("-q, --qtl     : draw also the QTL");
        System.out.println("--qmode       : QTL representation mode");
        System.out.println("-p, --par     : the file location of the drawing parameters");
        System.out.println("-t, --tonto   : the XML file for the trait ontology");
        System.out.println("-o, --outstem : the output stem");
        System.out.println("--tree : the tree which linked the QTL");
        System.out.println("--img         : the format of the image among this list");
        MetaMain.printImgFormat(15);       
	}
	
	public static void main(String[] args) {
		
		MapView program = new MapView();
		
		program.initCmdLineParser();
		
		CmdLineParser parser = program.parser;
		
        CmdLineParser.Option amapFile	= parser.addStringOption('m', "map");
        CmdLineParser.Option achromList	= parser.addStringOption('c', "chrom");
        CmdLineParser.Option awithQtl	= parser.addBooleanOption('q', "qtl");
        CmdLineParser.Option amodeQtl	= parser.addIntegerOption("qmode");
        CmdLineParser.Option aparFile	= parser.addStringOption('p', "par");
        CmdLineParser.Option aontoFile	= parser.addStringOption('t', "tonto");
        CmdLineParser.Option atreeFile	= parser.addStringOption("tree");
        CmdLineParser.Option aimgFormat	= parser.addStringOption("img");
        CmdLineParser.Option aoutFile	= parser.addStringOption('o', "outdir");
        
        program.parseCmdLine(args);
        
        String mapFile  	 =  (String)parser.getOptionValue(amapFile);
        String chromList  	 =  (String)parser.getOptionValue(achromList);
        Boolean withQtl 	 =  (Boolean)parser.getOptionValue(awithQtl, new Boolean(false));
        Integer modeQtl 	 =  (Integer)parser.getOptionValue(amodeQtl, new Integer(QtlPartition.VERTICAL_MODE));
        String ontoFile  	 =  (String)parser.getOptionValue(aontoFile);
        String treeFile  	 =  (String)parser.getOptionValue(atreeFile);
        String parFile  	 =  (String)parser.getOptionValue(aparFile);
        String imgFormat	 =  (String)parser.getOptionValue(aimgFormat, "jpeg");
        String outFile  	 =  (String)parser.getOptionValue(aoutFile);
        
        if (mapFile == null) {
        	System.err.println("[ ERROR ] : No map file defined");
        	System.exit(2);
		}
        if (chromList == null) {
        	System.err.println("[ ERROR ]:  No chromosome name defined");
        	System.exit(2);
		}
        if (outFile == null) {
        	System.err.println("[ ERROR ] : No output file defined");
        	System.exit(2);
        }
        
        IBioGenome map			= null;
        IBioOntology ontology   = null;
        MetaQtlAnalysis tree    = null;
        String[] chromNames     = null;
		
        try {
			
        	map  	     = getMap(mapFile);
        	ontology     = getOntology(ontoFile, 'x');
        	tree         = getResult(treeFile);
        	setParameter(parFile);
        	chromNames = parseChromList(chromList);
			
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			System.exit(3);
			
		}
		
		if (map == null) {
			System.err.println("[ ERROR ] : Unable to load map from " + mapFile);
			System.exit(4);
		}
		
	 	Chromosome[] chromosomes = ChromosomeAdapter.toChromosomes(map);
		Chromosome[] chrom2draws = getChromByNames(chromosomes, chromNames);
		if (chrom2draws == null) chrom2draws = chromosomes;
		ChromGraph[] chromGraphs = getChromGraph(chrom2draws, withQtl.booleanValue(), modeQtl.intValue(), ontology, tree);
		if (chromGraphs != null) {
			try {
				File file = new File(outFile+"."+imgFormat);
				writeImg(chromGraphs, file, imgFormat);
				file = new File(outFile+".par");
				writePar(file);
			} catch (IOException e) {
 				System.err.println(e.getMessage());
				System.exit(5);
			}
 			System.exit(0);
		}
		
	 	System.err.println("[ ERROR ] : No chromosome to draw !");
		System.exit(6);
	 	
	 }

	
	 
}
