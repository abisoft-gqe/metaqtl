/*  
 *  src/org/metaqtl/main/QTLTree.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.main;

import java.io.FileOutputStream;
import java.io.IOException;

import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.MetaQtlAnalysis;
import org.metaqtl.algo.HClustAlgorithm;
import org.metaqtl.algo.QtlTreeAlgorithm;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioOntology;
import org.metaqtl.factory.MetaQtlAnalysisFactory;

/**
 * @author jveyrier
 * 
 */
public final class QTLTree  extends MetaMain {
	
	private static final String VERSION = IMetaQtlConstants.VERSION; 
	
	private static final String syntax  =  "Syntaxe: QTLTree "+
											"[{-q, --qtlmap}]] "+
											"[{-o, --outstem}]] "+
											"[{-t, --tonto}]] "+
											"[{-m, --mode}]] "+
											"[{--cimode}] "+
											"[{--cimiss}] "+
											MetaMain.generalUsage();
	
	public void printUsage() {
        System.err.println(syntax);
    }
	
	public void printHelp() {
		
		System.out.println("QTLTree"+VERSION+", Copyright (C) 2005  Jean-Baptiste Veyrieras (INRA)");
		System.out.println("QTLTree comes with ABSOLUTELY NO WARRANTY.");
		System.out.println("This is free software, and you are welcome to redistribute it");
		System.out.println("under certain conditions;");
		System.out.println();
		System.out.println(syntax);
        System.out.println();
        MetaMain.generalHelp();
        System.out.println();
        System.out.println("-q, --qtlmap   : the file location of the qtl data base");
        System.out.println("-o, --outstem  : the stem name for output files");
        System.out.println("-t, --tonto    : the file location of the trait ontology");
        System.out.println("-m, --mode     : the clustering mode (see manual)");
        System.out.println("--cimode : the mode of computation of the QTL CI (see manual)");
        System.out.println("--cimiss : the mode of imputation for missing QTL CI (see manual)");
        
        
        
	}
	
	public static void main(String[] args) {
		
		QTLTree program = new QTLTree();
		
		program.initCmdLineParser();
		
		CmdLineParser parser = program.parser;
		
        CmdLineParser.Option aqtlFile  = parser.addStringOption('q', "qtlfile");
        CmdLineParser.Option aoutStem  = parser.addStringOption('o', "outstem");
        CmdLineParser.Option aontoFile = parser.addStringOption('t', "ontofile");
        CmdLineParser.Option amode     = parser.addIntegerOption('m', "mode");
        
        CmdLineParser.Option acimode = parser.addIntegerOption("cimode");
        CmdLineParser.Option acimiss = parser.addIntegerOption("cimiss");
        CmdLineParser.Option ahelp	 = parser.addBooleanOption("help");

        program.parseCmdLine(args);
        
        // Mandatory
        String qtlF  	 =  (String)parser.getOptionValue(aqtlFile);
        String outF  	 =  (String)parser.getOptionValue(aoutStem);
        // Optional
        String ontoF 	 =  (String)parser.getOptionValue(aontoFile, null);
        Integer cimode   =  (Integer)parser.getOptionValue(acimode, new Integer(IMetaQtlConstants.CI_MODE_AVAILABLE));
        Integer cimiss   =  (Integer)parser.getOptionValue(acimiss, new Integer(IMetaQtlConstants.CI_MISS_IMPUTATION));
        Integer mode    =   (Integer)parser.getOptionValue(amode, new Integer(HClustAlgorithm.WARD_METHOD));
        
        if (qtlF == null) {
        	System.err.println("No qtl data base file defined : EXIT");
        	System.exit(2);
		}
        if (outF == null) {
        	System.err.println("No out stem file defined : EXIT");
        	System.exit(3);
        }
        
        // Unmarshall the qtldb and the ontology
        IBioGenome qtldb		= null;
        IBioOntology ontology	= null;
        
		try {
			qtldb = getMap(qtlF);
	        if (ontoF != null) ontology = getOntology(ontoF, 'x');
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(3);
		}
		
        // Initialize the clustering algorithm
        QtlTreeAlgorithm algorithm = new QtlTreeAlgorithm(qtldb, ontology);
        if (!program.verbose.booleanValue())
        	algorithm.disableLogger();
        algorithm.setTreeMethod(mode.intValue());
        algorithm.setCIMode(cimode.intValue());
		algorithm.setCIMiss(cimiss.intValue());
		
		algorithm.run();
				
		MetaQtlAnalysis metaAnalysis = algorithm.getResult();
		
		try {
			FileOutputStream stream = new FileOutputStream(outF);
			MetaQtlAnalysisFactory.write(metaAnalysis, stream);
			stream.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(4);
		}
		
		System.exit(0);
	}
	
}
