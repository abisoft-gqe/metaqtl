/*  
 *  src/org/metaqtl/main/QTLClust.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.main;


import java.io.FileOutputStream;
import java.io.IOException;

import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.MetaQtlAnalysis;
import org.metaqtl.algo.EMAlgorithm;
import org.metaqtl.algo.QtlClustAlgorithm;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioOntology;
import org.metaqtl.factory.MetaQtlAnalysisFactory;
import org.metaqtl.factory.MetaQtlAnalysisModelFactory;
import org.metaqtl.factory.MetaQtlAnalysisSummaryFactory;

/**
 * 
 */
public final class QTLClust extends MetaMain {
	
	private static final String VERSION = IMetaQtlConstants.VERSION; 
	
	private static final String syntax  =  "Syntaxe: QTLClust "+
											"[{-q, --qtlmap}]] "+
											"[{-o, --outstem}]] "+
											"[{-t, --tonto}]] "+
											"[{-k, --kmax}] "+
											"[{--cimode}] "+
											"[{--cimiss}] "+
											"[{-c, --chr}] "+
											"[{--emrs}] "+
											"[{--emeps}] "+
											MetaMain.generalUsage();
	
	public void printUsage() {
        System.err.println(syntax);
    }
	
	public void printHelp() {
		
		System.out.println("QTLClust"+VERSION+", Copyright (C) 2005  Jean-Baptiste Veyrieras (INRA)");
		System.out.println("QTLClust comes with ABSOLUTELY NO WARRANTY.");
		System.out.println("This is free software, and you are welcome to redistribute it");
		System.out.println("under certain conditions;");
		System.out.println();
		System.out.println(syntax);
        System.out.println();
        MetaMain.generalHelp();
        System.out.println();
        System.out.println("-q, --qtlmap   : the file location of the qtl data base");
        System.out.println("-o, --outstem  : the stem name for output files");
        System.out.println("-t, --tonto    : the file location of the trait ontology (optional)");
        System.out.println("-c, --chr      : the chromosome to study (optional)");
        System.out.println("--cimode : the mode of computation of the QTL CI (see manual)");
        System.out.println("--cimiss : the mode of imputation for missing QTL CI (see manual)");
        System.out.println("--emrs  : the number of random starting points for the EM algorithm");
        System.out.println("--emeps : the convergence tolerance for the EM algorithm");
        System.out.println("-k, --kmax   : the maximum number of cluster partitions (optional)");
        
        
        
	}
	
	public static void main(String[] args) {
		
		QTLClust program = new QTLClust();
		
		program.initCmdLineParser();
		
		CmdLineParser parser = program.parser;
		
        CmdLineParser.Option aqtlFile  = parser.addStringOption('q', "qtlmap");
        CmdLineParser.Option aoutStem  = parser.addStringOption('o', "outstem");
        CmdLineParser.Option aontoFile = parser.addStringOption('t', "tonto");
        CmdLineParser.Option achrName  = parser.addStringOption('c', "chr");
        
        CmdLineParser.Option acimode = parser.addIntegerOption("cimode");
        CmdLineParser.Option acimiss = parser.addIntegerOption("cimiss");
        CmdLineParser.Option aemrs   = parser.addIntegerOption("emrs");
        CmdLineParser.Option aemeps  = parser.addDoubleOption("emeps");
        CmdLineParser.Option akmax	 = parser.addIntegerOption('k', "kmax");
        CmdLineParser.Option ahelp	 = parser.addBooleanOption("help");

        program.parseCmdLine(args);
        
        // Mandatory
        String qtlF  	 =  (String)parser.getOptionValue(aqtlFile);
        String outF  	 =  (String)parser.getOptionValue(aoutStem);
        // Optional
        String chrName	 =  (String)parser.getOptionValue(achrName);
        String ontoF 	 =  (String)parser.getOptionValue(aontoFile, null);
        Integer cimode   =  (Integer)parser.getOptionValue(acimode, new Integer(IMetaQtlConstants.CI_MODE_AVAILABLE));
        Integer cimiss   =  (Integer)parser.getOptionValue(acimiss, new Integer(IMetaQtlConstants.CI_MISS_IMPUTATION));
        Integer emrs     =  (Integer)parser.getOptionValue(aemrs, new Integer(EMAlgorithm.EM_START));
        Double  emeps    =  (Double)parser.getOptionValue(aemeps, new Double(EMAlgorithm.EM_ERR));
        Integer kmax     =  (Integer)parser.getOptionValue(akmax, new Integer(0));
        
        if (qtlF == null) {
        	System.err.println("No qtl data base file defined : EXIT");
        	System.exit(2);
		}
        if (outF == null) {
        	System.err.println("No out stem file defined : EXIT");
        	System.exit(3);
        }
        
        // Unmarshall the qtldb and the ontology
        IBioGenome qtldb		= null;
        IBioOntology ontology	= null;
        
		try {
			qtldb = getMap(qtlF);
	        if (ontoF != null) ontology = getOntology(ontoF, 'x');
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(3);
		}
		
        // Initialize the clustering algorithm
        QtlClustAlgorithm algorithm = new QtlClustAlgorithm(qtldb, chrName, ontology);
        
        if (!program.verbose.booleanValue()) {
        	algorithm.disableLogger();
        }
        
        // Set the max number of cluster if defined
        if (kmax.intValue() > 0)
        	algorithm.setMaxClusterNumber(kmax.intValue());		
		if (cimode.intValue() > 0)
			algorithm.setCIMode(cimode.intValue());
		if (cimiss.intValue() > 0)
			algorithm.setCIMiss(cimiss.intValue());
		if (emrs.intValue() > 0)
			algorithm.setEMRanStartNumber(emrs.intValue());
		if (emeps.doubleValue() > 0.0)
			algorithm.setEMEps(emeps.doubleValue());
			
		algorithm.run();
		
		MetaQtlAnalysis metaAnalysis = algorithm.getResult();
				
		try {
			
			FileOutputStream stream = new FileOutputStream(outF+"_res.txt");
			MetaQtlAnalysisFactory.write(metaAnalysis, stream);
			stream.close();
			stream = new FileOutputStream(outF+"_crit.txt");
			MetaQtlAnalysisSummaryFactory.write(metaAnalysis, stream);
			stream.close();
			stream = new FileOutputStream(outF+"_model.txt");
			MetaQtlAnalysisModelFactory.write(metaAnalysis, stream);
			stream.close();
			
		} catch (IOException e) {
			
			System.err.println(e.getMessage());
			System.exit(4);
			
		}
		
		System.exit(0);
	}

	
}
