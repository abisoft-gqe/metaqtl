/*  
 *  src/org/metaqtl/MetaDico.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

/**
 * This class represents a dictionnary of synonyms. In this context the entries
 * of the dictionary are standard names and the definition for each entry is just
 * a list of other terms which are synonyms.
 */
public class MetaDico {

	private Hashtable dico;
	
	private Hashtable reverse_dico;

	public MetaDico() {
		dico=new Hashtable();
		reverse_dico=new Hashtable();
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public String getTerm(String name)
	{
		if (reverse_dico.containsKey(name))
			return (String)reverse_dico.get(name);
		else if (dico.containsKey(name))
			return name;
		else return null;
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public List getSynonymous(String name) {
		if (dico.containsKey(name)) {
			return (List)dico.get(name);
		} else return null;
	}
	/**
	 * 
	 * @return
	 */
	public Enumeration getTerms() {
		return dico.keys();
	}
	/**
	 * 
	 * 
	 * @param name
	 * @param synonymous
	 */
	public void setTerm(String name, List synonymous) {
		dico.put(name, synonymous);
		Iterator iter = synonymous.iterator();
		while(iter.hasNext()) {
			reverse_dico.put(iter.next(), name);
		}
	}
	
	public void addTerm(String term, String syn) {
		ArrayList t;
		if (!dico.contains(term)) {
			t = new ArrayList();
			dico.put(term, t);			
		} else t = (ArrayList)dico.get(term);
		t.add(syn);
		reverse_dico.put(syn, term);
	}
}
