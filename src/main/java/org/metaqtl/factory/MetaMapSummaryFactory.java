/*  
 *  src/org/metaqtl/factory/MetaMapSummaryFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.factory;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.metaqtl.MetaChrom;
import org.metaqtl.MetaMap;
/**
 * A factory to write out the information on a {@link MetaMap}.
 */
public class MetaMapSummaryFactory {
	
	/**
	 * This methods writes out the summary for a consensus genetic map into
	 * a given output stream <code>stream</code>. The user is reponsible for
	 * closing the stream. 
	 * The format of the output is as follows :
	 * <ul>
	 * <li>CH the name of the chromosome.</li>
	 * <li>NM the number of markers on the chromosome.</li>
	 * <li>GF the goodness-of-fit.</li>
	 * <li>PV the p-value.</li>
	 * <li>DF the degress of freedom.</li>
	 * <li>SR the standard residuals for the marker intervals of the 
	 *    imput maps.</li>
	 * </ul>
	 * @param metaMap a consensus genetic map.
	 * @param stream an output stream.
	 */
	public static void write(MetaMap metaMap, OutputStream stream) throws IOException {
		int i,j,k,l;
		PrintWriter writer;
		
		writer = new PrintWriter(stream);
		
		for(i=0;i<metaMap.nchr;i++) {
			
			MetaChrom metaChrom = metaMap.chromosomes[i];
			
			if (metaChrom == null) continue;
			
			writer.println("CR " + metaChrom.name);
			writer.println("NM " + metaChrom.nm);
			writer.println("GF " + metaChrom.chi2);
			writer.println("PV " + metaChrom.pvalue);
			writer.println("DF " + metaChrom.dof);			
			for(l=j=0;j<metaChrom.nc;j++) {
				for(k=0;k<metaChrom.nmc[j]-1;k++) {
					writer.println("SR "+metaChrom.mapNames[j]+" "+metaChrom.rsd[l++]);
				}
			}
			
		}
		
		writer.flush();
		writer.close();
		
	}

}
