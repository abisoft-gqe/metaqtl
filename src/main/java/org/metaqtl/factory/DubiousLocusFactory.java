/*  
 *  src/org/metaqtl/factory/DubiousLocusFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.metaqtl.bio.IBioLocus;
import org.metaqtl.bio.entity.Locus;
/**
 * A factory to read a list of dubious markers from a file.
 */
public final class DubiousLocusFactory {
	/**
	 * This routine reads the list of dubious markers from
	 * the given file location and returns if succeed an
	 * array of <code>IBioLocus</code> objects. 
	 * 
	 * @param file the file locations.
	 * @return the array of dubious markers.
	 * @throws IOException
	 */
	public static IBioLocus[] read(Reader reader) throws IOException {
		
		if (reader == null) return null;
		
		ArrayList locusList   = null;
		IBioLocus locus       = null;
		IBioLocus[]      loci = null;
		BufferedReader buffer = new BufferedReader(reader);
		String line;
		
		while ((line = buffer.readLine()) != null) {
			StringTokenizer token = new StringTokenizer(line);
			if (token.countTokens() > 0) {
				locus = Locus.newLocus(IBioLocus.MARKER);
				locus.setName(token.nextToken());
				if (locusList==null) locusList = new ArrayList();
				locusList.add(locus);
			}
		}
		
		reader.close();
		
		if (locusList != null && locusList.size() > 0) {
			loci = new IBioLocus[locusList.size()];
			locusList.toArray(loci);
		}
		
		return loci;
	}

}
