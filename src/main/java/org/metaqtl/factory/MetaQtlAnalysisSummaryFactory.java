/*  
 *  src/org/metaqtl/factory/MetaQtlAnalysisSummaryFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.factory;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Iterator;

import org.metaqtl.MetaQtlAnalysis;
import org.metaqtl.MetaQtlResult;
import org.metaqtl.bio.util.NumberFormat;

/**
 * A factory to deal with QTL meta-analysis result.
 */
public class MetaQtlAnalysisSummaryFactory {
	
	/**
	 * This method writes out the summary for a givene QTL meta-analysis result
	 * <code>analysis</code> on the given output stream <code>stream</code>. The
	 * user is respondible for closing the <code>stream</code>.
	 * 
	 * The output is a tabulated format with the following columns :
	 * <ul>
	 * <li>Chromosome : the name of the chromosome.</li>
	 * <li>Trait  : the name of the trait or trait group.</li>
	 * <li>K : the number of cluster.</li>
	 * <li>Log : the log-likelihood of the clustering model.</li>
	 * <li>CLog : the complete log-likelihood of the clustering model.</li>
	 * <li>AIC : the Akaike criterion.</li>
	 * <li>AIC3 : the modified Akaike criteria.</li>
	 * <li>ICOMP : the Information Complexity criterion.</li>
	 * <li>BIC : the Bayesian Information criterion.</li>
	 * <li>AWE : the Average of Weighted Evidence criterion.</li> 
	 * </ul>
	 *
	 * @param analysis
	 * @param stream
	 * @throws IOException
	 */
	public static void write(MetaQtlAnalysis analysis, OutputStream stream) throws IOException {
		int nchr,i,j,l;
		MetaQtlResult[] results;
		PrintWriter writer;
		
		if (analysis == null) return;
		if (stream == null) return;
		
		writer = new PrintWriter(new OutputStreamWriter(stream));
		
		nchr = analysis.nchr;
		
		writer.print("Chromosome\t");
		writer.print("Trait\t");
		writer.print("K\t");
		writer.print("Criterion\t");
		writer.print("Value\t");
		writer.print("Delta\t");
		writer.println("Weight");
		
		for(i=0;i<nchr;i++) {
			
			results = analysis.resultByChrom[i];
			
			for(j=0;j<results.length;j++) {
				
				if (results[j] != null) {
					
					int[] K = results[j].getKValues();
					
					Iterator iter 	= results[j].getCriteria();
					
					while(iter.hasNext()) {
						
						String criterion = (String) iter.next();
						
						double[] values  = results[j].getCritValues(criterion, K);
						double[] delta   = results[j].getCritDelta(criterion, K);
						double[] weights = results[j].getCritWeights(criterion, K);
						
						for(l=0;l<K.length;l++) {
								
							write_line(writer, analysis.chromNames[i], results[j].trait, criterion, K[l], values[l], delta[l], weights[l]);
						}
						
					}
					
				}
			}
		}
		
		writer.flush();
		writer.close();
		
	}
	
	private static void write_line(PrintWriter writer, String chrom, String trait, String criterion, int k, double value, double delta, double weight) {
		writer.print(chrom+"\t");
		writer.print(trait+"\t");
		writer.print(k+"\t");
		writer.print(criterion+"\t");
		writer.print(NumberFormat.formatDouble(value)+"\t");
		writer.print(NumberFormat.formatDouble(delta)+"\t");
		writer.println(NumberFormat.formatDouble(weight));
		writer.flush();
	}
	
}
