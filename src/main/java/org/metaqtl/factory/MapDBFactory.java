/*  
 *  src/org/metaqtl/factory/MapDBFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.entity.GeneticMap;

/**
 * A factory to read the list and the properties of a set
 * of genetic maps. 
 */
public final class MapDBFactory {
	/**
	 * This method reads a file formated as follows :
	 * 
	 * map	property1  property2  property3 ...
	 *  
	 * when the fields are assumed to not contain blank space.
	 * 
	 * For each row of the input file (except the header which
	 * is mandatory) a IBioGenome is created and the array 
	 * returns should have the same length as the number of 
	 * rows in the file. 
	 *  
	 * @param file the file location.
	 * @return the array of genetic maps.
	 * @throws IOException
	 */
	public static IBioGenome[] read(Reader reader) throws IOException {
		if (reader == null) return null;
		
		int i,cnt;
		BufferedReader buffer=null;
		StringTokenizer token=null;
		String line=null,sbuff=null;
		String[] propKeys=null;
		IBioGenome[] maps = null;
		IBioGenome map=null;
		Properties properties=null;
		ArrayList mapList=null;
		
		buffer = new BufferedReader(reader);
		
		/* First read the header */
		line = buffer.readLine();
		if (line == null) return maps;
		token = new StringTokenizer(line);
		/* Check if the header is valid, i.e if the 
		 * first token is the map name
		 */
		if (token.countTokens() >= 1) {
			sbuff = token.nextToken();
			if (sbuff.equalsIgnoreCase("map")) {
				propKeys = new String[token.countTokens()];
				i=0;
				while(token.hasMoreTokens()) {
					propKeys[i++] = token.nextToken();
				}
			} else {
				throw new IOException("Bad header format : first field must be called 'map'");
			}
		}
		/* Now read the rows */
		cnt=1;
		while ((line = buffer.readLine()) != null) {
			token = new StringTokenizer(line);
			/* Test if the number of token equals to expected
			 * number.
			 */
			if (token.countTokens() == propKeys.length+1) {
				map = new GeneticMap();
				map.setName(token.nextToken());
				properties = map.getProperties();
				i=0;
				while(token.hasMoreTokens()) {
					properties.setProperty(propKeys[i++], token.nextToken());
				}
				map.setProperties(properties);
				if (mapList == null) mapList = new ArrayList();
				mapList.add(map);
			} else {
				throw new IOException("Bad line format at " + cnt);
			}
			cnt++;
		}
		
		buffer.close();
		reader.close();
		
		if (mapList != null) {
			maps = new IBioGenome[mapList.size()];
			mapList.toArray(maps);
		}
		
		return maps;
	}

	
}
