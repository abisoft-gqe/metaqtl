/*  
 *  src/org/metaqtl/factory/MetaQtlAnalysisModelFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.factory;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;

import org.metaqtl.EMCriteria;
import org.metaqtl.MetaQtlAnalysis;
import org.metaqtl.MetaQtlModel;

/**
 * 
 */
public class MetaQtlAnalysisModelFactory {
	/**
	 * 
	 * @param analysis
	 * @param stream
	 * @throws IOException
	 */
	public static void write(MetaQtlAnalysis analysis, OutputStream stream) throws IOException {
		PrintWriter writer = new PrintWriter(stream);
		write(analysis,writer);
	}
	
	public static void write(MetaQtlAnalysis analysis, Writer writer) throws IOException {
		
		if (analysis == null) return;
		if (writer   == null) return;
		
		MetaQtlModel model;
		ArrayList modelList;
		
		// Get the best model for each criterion.
		Iterator iter 	= EMCriteria.getCriteria();
		modelList 		= new ArrayList();
		while(iter.hasNext()) {
			String critName = (String)iter.next();
			int crit		= EMCriteria.getCriterionIdx(critName);
			model = analysis.getBestModel(crit);
			model.setCriterion(critName);
			modelList.add(model);
		}
		// Print out the models
		iter = modelList.iterator();
		while(iter.hasNext()) {
			model = (MetaQtlModel) iter.next();
			MetaQtlModelFactory.write(model, writer);
		}
	}

}
