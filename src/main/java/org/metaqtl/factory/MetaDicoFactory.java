/*  
 *  src/org/metaqtl/factory/MetaDicoFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.StringTokenizer;

import org.metaqtl.MetaDico;

/**
 * 
 */
public class MetaDicoFactory {
	
	/**
	 * 
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	public static MetaDico read(Reader reader) throws IOException {
		if (reader == null) return null;
		
		int cnt;
		BufferedReader buffer=null;
		StringTokenizer token=null;
		String line=null;
		MetaDico dico=null;
		
		
		buffer = new BufferedReader(reader);
		
		/* First read the header */
		line = buffer.readLine();
		if (line == null) return null;
		
		token = new StringTokenizer(line);
		dico  = new MetaDico();
		/* Now read the rows */
		cnt=1;
		while ((line = buffer.readLine()) != null) {
			token = new StringTokenizer(line, "\t");
			/* Test if the number of token equals to expected
			 * number.
			 */
			if (token.countTokens() == 2) {
				dico.addTerm(token.nextToken(), token.nextToken());				
			} else {
				throw new IOException("Bad line format at " + cnt);
			}
			cnt++;
		}
		
		buffer.close();
		reader.close();
		
		return dico;
	}

}
