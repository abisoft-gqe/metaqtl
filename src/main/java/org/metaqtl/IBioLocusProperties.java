/*  
 *  src/org/metaqtl/IBioLocusProperties.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

/**
 * This interface defines the key of the properties which
 * can be handled by a org.thalia.bio.entity.IBioLocus.
 */
public interface IBioLocusProperties {
	
	/**
	 * The key for the occurence of markers in a consensus map
	 */
	public static String OCCURENCE 			 = "meta.occurence";
	/**
	 * The key for the QTL trait name.
	 */
	public static String QTL_TRAIT_NAME 	 = "qtl.trait.name";
	/**
	 * The key for the QTL trait unit.
	 */
	public static String QTL_TRAIT_UNIT 	 = "qtl.trait.unit";
	/**
	 * The key for the QTL trait group.
	 */
	public static String QTL_TRAIT_GROUP 	 = "qtl.trait.group";
	/**
	 * The key for the QTL confidence interval LOD decrease.
	 */
	public static String QTL_CI_LOD_DECREASE = "qtl.ci.lod.decrease";
	/**
	 * The key for the QTL confidence interval lower boundary.
	 */
	public static String QTL_CI_FROM 		 = "qtl.ci.from";
	/**
	 * THe key for the QTL confidence interval upper boundary.
	 */
	public static String QTL_CI_TO	 		 = "qtl.ci.to";
	/**
	 * The key for the QTL rsquare.
	 */
	public static String QTL_STATS_RSQUARE	 = "qtl.rsquare";
	/**
	 * The key for the QTL additive effect.
	 */
	public static String QTL_STATS_ADDITIVE	 = "qtl.effect.additive";
	/**
	 * The key for the QTL dominance effect.
	 */
	public static String QTL_STATS_DOMINANCE = "qtl.effect.dominance";
	/**
	 * The key for the QTL cross name.
	 */
	public static String QTL_CROSS_NAME		 = "qtl.cross.name";
	/**
	 * The key for the QTL cross size.  
	 */
	public static String QTL_CROSS_SIZE		 = "qtl.cross.size";
	/**
	 * The key for the QTL cross type.
	 */
	public static String QTL_CROSS_TYPE		 = "qtl.cross.type";
	/**
	 * The key for the marker dubious status.
	 */
	public static String DUBIOUS_ORDER 		 = "dubious.order";
	/**
	 * The key for the QTL flanking marker interval projection
	 * rate.
	 */
	public static String QTL_PROJ_INTSCALE   = "qtl.proj.intScale";
	/**
	 * The key for the QTL confidence interval projection rate.
	 */
	public static String QTL_PROJ_MAPSCALE   = "qtl.proj.mapScale";
	/**
	 * The key for the QTL flanking marker interval order preservation.
	 */
	public static String QTL_PROJ_SHAREFLANKING = "qtl.proj.shareFlanking";
	/**
	 * The key for the QTL flanking marker interval reverse order.
	 */
	public static String QTL_PROJ_SWAPFLANKING = "qtl.proj.swapFlanking";
	/**
	 * The key for the map of origin of the QTL.
	 */
	public static String QTL_PROJ_ORIGIN = "qtl.proj.mapOrigin";
	
}
