/*  
 *  src/org/metaqtl/algo/MetaAlgorithm.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.algo;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Writer;

import org.metaqtl.IMetaAlgorithm;

/**
 * 
 */
public abstract class MetaAlgorithm implements IMetaAlgorithm {
	/**
	 * The amount of work to do.
	 */
	protected int workAmount = 0;
	/**
	 * The amount of work done.
	 */
	protected int workProgress = 0;
	/**
	 * The logger.
	 */
	protected PrintStream logger = null;
	/**
	 * The logger status.
	 */
	protected boolean loggerUp = true;
	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.IMetaAlgorithm#getWorkAmount()
	 */
	public int getWorkAmount() {
		return workAmount;
	}

	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.IMetaAlgorithm#getWorkProgress()
	 */
	public int getWorkProgress() {
		return workProgress;
	}

	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.IMetaAlgorithm#setLogger(java.io.Writer)
	 */
	public void setLogger(Writer writer) {
		//TODO this.logger = writer;
	}

	/* (non-Javadoc)
	 * @see org.thalia.metaqtl.IMetaAlgorithm#setLogger(java.io.OutputStream)
	 */
	public void setLogger(OutputStream stream) {
		this.logger = new PrintStream(stream);
	}
	/**
	 * Returns the logger for this algorithm as a 
	 * <code>PrintStream</code>. If any logger has
	 * been previously defined then the method returns
	 * the default logger, i.e <code>System.err</code>
	 * 
	 * @return
	 */
	protected PrintStream getLogger() {
		if (logger == null && loggerUp)
			return System.err;
		else return logger;
	}
	/**
	 * 
	 */
	public void disableLogger() {
		loggerUp=false;
	}
	
	public boolean isLoggerEnable() {
		return loggerUp;
	}

}
