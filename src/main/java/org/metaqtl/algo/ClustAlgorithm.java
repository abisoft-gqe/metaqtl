/*  
 *  src/org/metaqtl/algo/ClustAlgorithm.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.algo;

import org.metaqtl.Chromosome;
import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.MetaQtlAnalysis;
import org.metaqtl.adapter.ChromosomeAdapter;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioOntology;

/**
 * 
 */
public abstract class ClustAlgorithm extends MetaAlgorithm {
	/**
	 * The result of the clustering.
	 */
	protected MetaQtlAnalysis result;
	/**
	 * The chromosome on which the clustering is performed.
	 */
	protected Chromosome[] chromosomes;
	/**
	 * The optional trait ontology to use to group Qtl.
	 */
	protected IBioOntology ontology;
	/**
	 * The name of the chromosome to study.
	 */
	protected String chrName;
	/**
	 * This global variable defines the mode of compuation of
	 * the standard deviation of the QTL positions.
	 */
	protected  int sd_mode = IMetaQtlConstants.CI_MODE_MAXIMAL;
	/**
	 * This global variable defines the mode of imputation
	 * for the QTL for which the standard deviation of the position
	 * is not available.   
	 */
	protected int missing_sd_mode = IMetaQtlConstants.CI_MISS_IMPUTATION;
	
	/**
	 * 
	 * @param map
	 * @param ontology
	 */
	public ClustAlgorithm(IBioGenome map, IBioOntology ontology) {
		init(map,ontology);
	}
	
	public ClustAlgorithm(IBioGenome map, String name, IBioOntology ontology) {
		init(map,ontology);
		if (name!=null) chrName = new String(name);
	}
	
	/**
	 * 
	 * @param sd_mode
	 */
	public void setCIMode(int sd_mode) {
		this.sd_mode = sd_mode;
	}
	
	private void init(IBioGenome map, IBioOntology ontology) {
		if (map==null) return;
		
		chromosomes 	 = ChromosomeAdapter.toChromosomes(map);
		this.ontology    = ontology;
		
		/* Compute the amount of work */
		for(int i=0;i<chromosomes.length;i++) {
			if (chromosomes[i].hasQTL()) {
				workAmount++;
			}
		}
	}
	/**
	 * 
	 * @param missing_sd_mode
	 */
	public void setCIMiss(int missing_sd_mode) {
		this.missing_sd_mode = missing_sd_mode;
	}
	/**
	 * When the algorithm is finished use this method to get
	 * the result. It returns a {@link  MetaQtlAnalysis} instance
	 * which store in each chromosome the different clustering models
	 * which have been tested.
	 * 
	 * @return a instance of <code>MetaQtlAnalysis</code>.
	 * 
	 * @see MetaQtlAnalysis
	 */
	public MetaQtlAnalysis getResult() {
		return result;
	}

}
