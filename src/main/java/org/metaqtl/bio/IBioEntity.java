/*  
 *  src/org/thalia/bio/IBioEntity.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio;

import java.util.Properties;


/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public interface IBioEntity extends IBioAdaptable {	
	
	/**
	 * Returns the name of the biological entity
	 */
	public String getName();

	public void setName(String value);

	/**
	 * There are 2 main class of entities. The first one deals with population 
	 * biological entity, i.e population itself and individuals. The second class
	 * is a representation of microscopic biological entity from genome container
	 * to alleles.
	 * 
	 */
	public int getType();

	/**
	 * Returns the entity that handles this entity.
	 */
	public IBioEntity getParent();
	
	/**
	* Gets the properties of the entity.
	*/
	public Properties getProperties();	
	/**
	 * Sets the properties of the entity.
	 * @param defaultProps
	 */
	public void setProperties(Properties defaultProps);

}
