/*  
 *  src/org/thalia/bio/entity/GroupContainer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioLGroup;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class GroupContainer extends BioEntityContainer implements IBioGenome {

	/**
	 * 
	 */
	public GroupContainer() {super();}

	/**
	 * @param name
	 * @param parent
	 */
	public GroupContainer(String name, IBioEntity parent) {
		super(name, parent);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioGenome#groups()
	 */
	public IBioLGroup[] groups() {
		IBioEntity[] entities  = entities();
		IBioLGroup[] groups = new IBioLGroup[entities.length];
		for(int i=0; i<groups.length;i++)
			groups[i] = (IBioLGroup) entities[i];
		return groups;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioGenome#getGroup(java.lang.String)
	 */
	public IBioLGroup getGroup(String name) {
		return (IBioLGroup) getEntity(name);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioGenome#addGroup(org.cookqtl.bio.entity.IBioLGroup)
	 */
	public void addGroup(IBioLGroup group) {
		addEntity(group);	
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioGenome#removeGroup(java.lang.String)
	 */
	public void removeGroup(String name) {
		removeEntity(name);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioGenome#groupNumber()
	 */
	public int groupNumber() {
		return entityNumber();
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.extend.entity.BioEntity#getType()
	 */
	public abstract int getType();

}
