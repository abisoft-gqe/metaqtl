/*  
 *  src/org/thalia/bio/entity/BioEntity.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import java.util.Properties;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioEntityTypes;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class BioEntity implements IBioEntity {
	
	protected String name = "";
	
	protected IBioEntity parent = null;
	
	protected Properties properties;
	
	public BioEntity() {
		this.properties = new Properties();
	}
	
	public BioEntity(String name, IBioEntity parent) {
		this.name = name;
		this.parent = parent;
		this.properties = new Properties(); 
	}
	/* (non-Javadoc)
	 * @see org.cookqtl.core.bio.entity.IBioEntity#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.core.bio.entity.IBioEntity#setName(java.lang.String)
	 */
	public void setName(String value) {
		name = value;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.core.bio.entity.IBioEntity#getType()
	 */
	public abstract int getType();

	/* (non-Javadoc)
	 * @see org.cookqtl.core.bio.entity.IBioEntity#getParent()
	 */
	public IBioEntity getParent() {
		return (parent == null) ? this : parent;
	}
	
	public Properties getProperties() {
		return properties;
	}
	
	public void setProperties(Properties defaultProp) {
		properties = defaultProp;
	}
	/**
	 * 
	 */
	public static IBioEntity newBioEntity(int type) {
		if (type == IBioEntityTypes.MARKERLOCUS) 
			return new Marker();
		if (type == IBioEntityTypes.ALLELE)
			return null;
		if (type == IBioEntityTypes.COLLECTION)
			return new BioEntityCollection();
		if (type == IBioEntityTypes.GENOME)
			return new GeneticMap();
		if (type == IBioEntityTypes.INDIVIDUAL)
			return new Individual();
		if (type == IBioEntityTypes.LGROUP)
			return new LGroup();
		if (type == IBioEntityTypes.POPULATION)
			return new CrossPopulation();
		if (type == IBioEntityTypes.QTLLOCUS)
			return new QTL();
		if (type == IBioEntityTypes.ONTOLOGY)
			return new Ontology();
		if (type == IBioEntityTypes.ONTOLOGY_TERM)
			return new OntologyTerm();
		return null;
	}
	
	
	
}
