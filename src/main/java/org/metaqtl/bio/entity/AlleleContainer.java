/*  
 *  src/org/thalia/bio/entity/AlleleContainer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAllele;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioLGroup;
import org.metaqtl.bio.IBioLocus;
import org.metaqtl.bio.util.ILGroupPosition;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class AlleleContainer extends BioEntityContainer implements IBioLocus {

	/**
	 * 
	 */
	public AlleleContainer() {super();}

	/**
	 * @param name
	 * @param parent
	 */
	public AlleleContainer(String name, IBioEntity parent) {
		super(name, parent);
	}
	
	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public abstract int getType();
	
	public IBioAllele[] alleles() {
		return (IBioAllele[]) entities();
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#addAllele(org.cookqtl.bio.entity.IBioAllele)
	 */
	public void addAllele(IBioAllele allele) {
		addEntity(allele);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#removeAllele(java.lang.String)
	 */
	public void removeAllele(String name) {
		removeEntity(name);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#getAllele(java.lang.String)
	 */
	public IBioAllele getAllele(String name) {
		return (IBioAllele) getEntity(name);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#getGroup()
	 */
	public IBioLGroup getGroup() {
		return (IBioLGroup) getParent();
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#setGroup(org.cookqtl.bio.entity.IBioLGroup)
	 */
	public void setGroup(IBioLGroup group) {
		parent = group;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#getAlleleNumber()
	 */
	public int getAlleleNumber() {
		return entityNumber();
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#getPosition()
	 */
	public abstract ILGroupPosition getPosition();

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#setPosition(org.cookqtl.bio.util.ILGroupPosition)
	 */
	public abstract void setPosition(ILGroupPosition position) ;
}
