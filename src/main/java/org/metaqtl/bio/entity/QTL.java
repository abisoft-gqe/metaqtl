/*  
 *  src/org/thalia/bio/entity/QTL.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.IBioLocus;
import org.metaqtl.bio.entity.adapter.LocusBeanAdapter;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class QTL extends Locus implements IBioLocus {

	
	/**
	 * 
	 */
	public QTL() {super();}

	/**
	 * @param name
	 * @param parent
	 */
	public QTL(String name, IBioEntity parent) {
		super(name, parent);
		
	}
	
	public int getType() {
	
	return IBioEntityTypes.QTLLOCUS;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdaptable#getBeanAdapter()
	 */
	public IBioAdapter getBioAdapter() {
		return new LocusBeanAdapter();
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioLocus#getLocusType()
	 */
	public int getLocusType() {
		return IBioLocus.QTL;
	}

	

}
