/*  
 *  src/org/thalia/bio/entity/Population.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.IBioIndividual;
import org.metaqtl.bio.IBioPopulation;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class Population extends IndividualContainer implements IBioPopulation {

	/**
	 * 
	 */
	public Population() {super();}

	protected int size;
	
	protected int generation;
	
	public  IBioIndividual[] parents;
	
	/**
	 * @param name
	 * @param parent
	 */
	public Population(String name, IBioEntity parent) {
		super(name, parent);
	}
	
	public static Population newCrossPopulation(int type, String name, IBioEntity parent) {
		return new CrossPopulation(name, parent, type);
	}
	
	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public int getType() {
		return IBioEntityTypes.POPULATION;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioPopulation#getIndividualNumber()
	 */
	public int getIndividualNumber() {
		return size;
	}
	
    /**
     * Set the population size.
     * @param size The size to set.
     */
    public void setSize(int size) {
        this.size = size;
    }
	/**
	 * Returns the size of the population.
	 * @return
	 */
	public int getSize() {
		return size;
	}
	
	public IBioAdapter getBioAdapter() {
		return null;
	}
	/**
	 * @return Returns the generation.
	 */
	public int getGeneration() {
		return generation;
	}
	/**
	 * @param generation The generation to set.
	 */
	public void setGeneration(int generation) {
		this.generation = generation;
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioCross#getParents()
	 */
	public IBioIndividual[] getParents() {
		// TODO Auto-generated method stub
		return null;
	}
}
