/*  
 *  src/org/thalia/bio/entity/BioEntityCollection.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.entity.adapter.BioEntityCollectionAdapter;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class BioEntityCollection extends ArrayList implements IBioEntity, Collection {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String name;
	
	protected IBioEntity parent;
	
	protected Properties properties;
	/**
	 * 
	 */
	public BioEntityCollection() {
		super();
	}

	/**
	 * @param i
	 */
	public BioEntityCollection(int i) {
		super(i);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public int getType() {
		return IBioEntityTypes.COLLECTION;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdaptable#getBeanAdapter()
	 */
	public IBioAdapter getBioAdapter() {
		return new BioEntityCollectionAdapter();
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#setName(java.lang.String)
	 */
	public void setName(String value) {
		name = value;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getParent()
	 */
	public IBioEntity getParent() {
		return parent;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getProperties()
	 */
	public Properties getProperties() {
		return properties;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#setProperties(java.util.Properties)
	 */
	public void setProperties(Properties defaultProps) {
		properties = new Properties(defaultProps);
	}


}
