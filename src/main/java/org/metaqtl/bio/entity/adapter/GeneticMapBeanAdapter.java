/*  
 *  src/org/thalia/bio/entity/adapter/GeneticMapBeanAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.adapter;

import java.util.ArrayList;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.entity.GeneticMap;
import org.metaqtl.bio.entity.LGroup;
import org.metaqtl.bio.entity.bean.GeneticMapBean;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class GeneticMapBeanAdapter extends BioEntityAdapter	implements IBioAdapter {

	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdapter#fromBean(java.lang.Object)
	 */
	public IBioEntity toEntity(Object bean) {
		
		if (bean instanceof GeneticMapBean) {
		
			GeneticMapBean geneticMapBean = (GeneticMapBean) bean;
			GeneticMap geneticMap = new GeneticMap(geneticMapBean.name);
			
			BioEntityAdapter.toEntity(geneticMapBean, geneticMap);
				
			for (int i = 0; i < geneticMapBean.groups.size(); i++) {
				
				Object groupBean = geneticMapBean.groups.get(i);
				
				LGroup group = new LGroup();
				
				group = (LGroup) group.getBioAdapter().toEntity(groupBean);
				
				group.setGenome(geneticMap);
				
				geneticMap.addGroup(group);
			
			}
			return geneticMap;
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdapter#toBean(java.lang.Object)
	 */
	public Object fromEntity(IBioEntity obj)  {
		
		if (obj instanceof org.metaqtl.bio.entity.GeneticMap) {
		
			GeneticMap map = (GeneticMap) obj;
			GeneticMapBean mapBean = new GeneticMapBean();
			
			BioEntityAdapter.fromEntity(map, mapBean);
			
			mapBean.name = map.getName();
			mapBean.groups = new ArrayList(map.groupNumber());
			IBioEntity[] groups = (IBioEntity[]) map.groups();

			for (int i = 0; i < groups.length; i++) {
				IBioAdapter adapter = groups[i].getBioAdapter();
				Object bean = adapter.fromEntity(groups[i]);
				mapBean.groups.add(i, bean);
			}

			return mapBean;
		}
		// Throw a new Core Exception
		// TODO
		return null;
	}

}
