/*  
 *  src/org/thalia/bio/entity/adapter/LGroupBeanAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.adapter;

import java.util.ArrayList;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioLocus;
import org.metaqtl.bio.entity.LGroup;
import org.metaqtl.bio.entity.Locus;
import org.metaqtl.bio.entity.bean.LGroupBean;
/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class LGroupBeanAdapter extends BioEntityAdapter implements IBioAdapter {

	public IBioEntity toEntity(Object bean)  {
		
		if (bean instanceof LGroupBean) {
			
			LGroupBean groupBean = (LGroupBean) bean;
			LGroup group = new LGroup(groupBean.name, null);
			
			BioEntityAdapter.toEntity(groupBean, group);
			
			for(int i=0; i<groupBean.locus.size(); i++) {
				
				Object locusBean = groupBean.locus.get(i);
				
				Locus locus = (Locus) Locus.getLocusAdapter().toEntity(locusBean);
				
				locus.setGroup(group);
				
				group.addLocus(locus);	
				//System.out.println(""+group.loci().length);			
			}	
			
			return group;
			
		}
		
		return null;
	}

	public Object fromEntity(IBioEntity obj)  {
		
		if (obj instanceof org.metaqtl.bio.entity.LGroup) {
		
			LGroup group = (LGroup) obj;
			LGroupBean groupBean = new LGroupBean();
			
			BioEntityAdapter.fromEntity(group, groupBean);
			
			groupBean.locus = new ArrayList(group.getLocusNumber());
			
			IBioLocus[] loci = group.loci();
			
			for (int i = 0; i < loci.length; i++) {
				
				IBioAdapter adapter = loci[i].getBioAdapter();
				
				Object bean = adapter.fromEntity(loci[i]);
				
				groupBean.locus.add(i, bean);	
			}
			
			return groupBean;
		}
		
		return null;
	}

}
