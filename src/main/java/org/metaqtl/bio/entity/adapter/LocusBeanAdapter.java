/*  
 *  src/org/thalia/bio/entity/adapter/LocusBeanAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.adapter;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.entity.Locus;
import org.metaqtl.bio.entity.bean.LocusBean;
import org.metaqtl.bio.util.BioLocusTypes;
import org.metaqtl.bio.util.LGroupPosition;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class LocusBeanAdapter extends BioEntityAdapter {

	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdapter#fromBean(java.lang.Object)
	 */
	public IBioEntity toEntity(Object bean) {
		
		if (bean instanceof LocusBean) {
			
			LocusBean locusBean = (LocusBean) bean;
			
			Locus locus = Locus.newLocus(locusBean.type);
			
			BioEntityAdapter.toEntity(locusBean, locus);
			
			if (locusBean.stderr==0.0) {
				locus.setPosition(
					LGroupPosition.newStaticLocusPosition(locusBean.position));
			} else {
				locus.setPosition(
						LGroupPosition.newEstimatedLocusPosition(locusBean.position, locusBean.stderr));
			}
			
			return locus;
			
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdapter#toBean(java.lang.Object)
	 */
	public Object fromEntity(IBioEntity obj) {
		if (obj instanceof org.metaqtl.bio.entity.Locus) {

			Locus locus = (Locus) obj;
			LocusBean markerBean = new LocusBean();

			BioEntityAdapter.fromEntity(locus, markerBean);

			if (locus.getPosition() != null) {
				markerBean.position = locus.getPosition().absolute();
				markerBean.stderr   = locus.getPosition().standardError();
			}
			
			markerBean.type = BioLocusTypes.typeToString(locus.getLocusType());
			
			
			return markerBean;
		}
		
		return null;
	}

}
