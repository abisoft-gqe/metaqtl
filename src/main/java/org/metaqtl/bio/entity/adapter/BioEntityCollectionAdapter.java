/*  
 *  src/org/thalia/bio/entity/adapter/BioEntityCollectionAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.adapter;

import java.util.Iterator;

import org.metaqtl.bio.IBioAdaptable;
import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.entity.BioEntityCollection;
import org.metaqtl.bio.entity.bean.BioEntityCollectionBean;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class BioEntityCollectionAdapter
	extends BioEntityAdapter
	implements IBioAdapter {

	public Object fromEntity(IBioEntity obj)  {
		
		BioEntityCollectionBean bean = new BioEntityCollectionBean();
		
		try{
			
			BioEntityCollection list = (BioEntityCollection) obj;
			
			BioEntityAdapter.fromEntity(list, bean);
			
			Iterator iter = list.iterator();
			
			while(iter.hasNext()) {
			
				IBioEntity entity = (IBioEntity) iter.next();
				bean.entities.add(entity.getBioAdapter().toEntity(entity));
				
			}
			
			return bean;
			
		} catch (ClassCastException e) {
			// throw a CoreException
		}
		return null;					
	}

	public IBioEntity toEntity(Object bean) {
		
		try{
			
			BioEntityCollectionBean collectionBean = (BioEntityCollectionBean) bean;
			BioEntityCollection          collection          = new BioEntityCollection();
			
			BioEntityAdapter.toEntity(collectionBean, collection);
			
			Iterator iter = collectionBean.entities.iterator();
			
			while(iter.hasNext()) {
				
				IBioAdaptable bean_iter = (IBioAdaptable)iter.next();
				
				collection.add(bean_iter.getBioAdapter().toEntity(bean_iter));
			}
			
			return collection; 
			
		} catch(ClassCastException e) {
			e.printStackTrace();
			
		}
		return null;
	}

}
