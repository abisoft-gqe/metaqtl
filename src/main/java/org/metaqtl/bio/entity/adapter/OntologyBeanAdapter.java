/*  
 *  src/org/thalia/bio/entity/adapter/OntologyBeanAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.adapter;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioOntologyTerm;
import org.metaqtl.bio.entity.Ontology;
import org.metaqtl.bio.entity.OntologyTerm;
import org.metaqtl.bio.entity.bean.OntologyBean;
import org.metaqtl.bio.entity.bean.OntologyTermBean;

/**
 * 
 */
public class OntologyBeanAdapter extends BioEntityAdapter  {

	/* (non-Javadoc)
	 * @see org.thalia.core.runtime.IBeanAdapter#toBean(java.lang.Object)
	 */
	public IBioEntity toEntity(Object obj)  {
		
		Ontology ot = null;
		
		if (obj instanceof OntologyBean) {
			
			OntologyBean otb = (OntologyBean) obj;
			
			ot = new Ontology();
			
			BioEntityAdapter.toEntity(otb, ot);
			
			ot.setFunction(otb.function);
			
			if (otb.root != null) {
				
				OntologyTerm root = new OntologyTerm();
				
				root = (OntologyTerm) root.getBioAdapter().toEntity(otb.root);
				
				ot.setRoot(root);
				
			}
			
		}
		
		return ot;
	}

	/* (non-Javadoc)
	 * @see org.thalia.core.runtime.IBeanAdapter#fromBean(java.lang.Object)
	 */
	public Object fromEntity(IBioEntity obj) {
		
		OntologyBean otb = null;
		
		if (obj instanceof Ontology) {
			
			Ontology ot = (Ontology) obj;
			
			otb = new OntologyBean();
			
			BioEntityAdapter.fromEntity(ot, otb);
			
			otb.function = ot.getFunction();			
			
			if (ot.getRoot() != null) {
				
				IBioOntologyTerm root = ot.getRoot();
				
				otb.root = (OntologyTermBean) root.getBioAdapter().fromEntity(root);
				
			}
			
		}
		
		return otb;
	}
	
}
