/*  
 *  src/org/thalia/bio/entity/adapter/OntologyTermBeanAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.adapter;

import java.util.ArrayList;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioOntologyTerm;
import org.metaqtl.bio.entity.OntologyTerm;
import org.metaqtl.bio.entity.bean.OntologyTermBean;

/**
 * 
 */
public class OntologyTermBeanAdapter extends BioEntityAdapter {

	/* (non-Javadoc)
	 * @see org.thalia.core.runtime.IBeanAdapter#toBean(java.lang.Object)
	 */
	public IBioEntity toEntity(Object obj)  {
		
		OntologyTerm ot = null;
		
		if (obj instanceof OntologyTermBean) {
			OntologyTermBean otb = (OntologyTermBean) obj;
			
			ot = new OntologyTerm();
			
			BioEntityAdapter.toEntity(otb, ot);
			
			ot.setID(otb.id);
			
			if (otb.children != null) {
				
				OntologyTerm[] children = new OntologyTerm[otb.children.size()];
				
				for(int i=0;i<otb.children.size();i++) {
					children[i] = new OntologyTerm(); 
					children[i] = (OntologyTerm) children[i].getBioAdapter().toEntity(otb.children.get(i));
					children[i].setParentTerm(ot);
				}
				
				ot.setChildren(children);
			}
			
		}
		
		return ot;
	}

	/* (non-Javadoc)
	 * @see org.thalia.core.runtime.IBeanAdapter#fromBean(java.lang.Object)
	 */
	public Object fromEntity(IBioEntity obj)  {
		
		OntologyTermBean otb = null;
		
		if (obj instanceof OntologyTerm) {
			
			OntologyTerm ot = (OntologyTerm) obj;
			
			otb = new OntologyTermBean();
			
			BioEntityAdapter.fromEntity(ot, otb);
			
			otb.id = ot.getID();
			
			if (ot.hasChildren()) {
				
				IBioOntologyTerm[] children = ot.getChildrenTerm();
				
				otb.children = new ArrayList(children.length);
				
				for(int i=0;i<children.length;i++) 
					otb.children.add(children[i].getBioAdapter().fromEntity(children[i]));
			}
			
		}
		
		return otb;
	}

}
