/*  
 *  src/org/thalia/bio/entity/adapter/BioEntityAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.adapter;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.entity.bean.BioEntityBean;
import org.metaqtl.bio.entity.bean.PropertyBean;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class BioEntityAdapter implements IBioAdapter {

	public static void toEntity(BioEntityBean bean, IBioEntity entity) {
		entity.setName((bean.name == null) ? "" : bean.name);
		if (bean.properties != null) {
			Properties properties = new Properties();
			Iterator iter = bean.properties.iterator();
			while(iter.hasNext()) {				
				PropertyBean property = (PropertyBean) iter.next();
				properties.setProperty(property.name, property.value);
				
			}
			entity.setProperties(properties);
		} else {
			entity.setProperties(new Properties());
		}
		
	}
	
	public static void fromEntity(IBioEntity entity, BioEntityBean bean) {
		bean.name = (entity.getName() == null) ? "" : entity.getName() ;
		Properties properties = (entity.getProperties() == null) ? new Properties() : entity.getProperties();
		bean.properties = new ArrayList(properties.size());
		Enumeration keys = properties.propertyNames();
		while(keys.hasMoreElements()) {
			PropertyBean property = new PropertyBean();			
			property.name = ((String)keys.nextElement());
			property.value = properties.getProperty(property.name);
			bean.properties.add(property);
		}
	}

}
