/*  
 *  src/org/thalia/bio/entity/OntologyTerm.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.IBioOntologyTerm;
import org.metaqtl.bio.entity.adapter.OntologyTermBeanAdapter;

/**
 * 
 */
public class OntologyTerm extends BioEntity implements IBioOntologyTerm {
	/**
	 * The id of the term.
	 */
	private long id;
	/**
	*  The parent of the term.
	*/
	private IBioOntologyTerm parentTerm = null;
	/**
	 * The children of the term.
	 */
	private IBioOntologyTerm[] childrenTerm = null;
	
	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioOntologyTerm#getID()
	 */
	public long getID() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioOntologyTerm#getChildrenTerm()
	 */
	public IBioOntologyTerm[] getChildrenTerm() {
		return childrenTerm;
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioOntologyTerm#hasChildren()
	 */
	public boolean hasChildren() {
		return (childrenTerm!=null);
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.extend.entity.BioEntity#getType()
	 */
	public int getType() {
		return IBioEntityTypes.ONTOLOGY_TERM;
	}

	/* (non-Javadoc)
	 * @see org.thalia.core.runtime.IBeanAdaptable#getBeanAdapter()
	 */
	public IBioAdapter getBioAdapter() {
		return new OntologyTermBeanAdapter();
	}

	/**
	 * Sets the indentifient of the term.
	 * 
	 * @param id the new identifient of the term. 
	 */
	public void setID(long id) {
		this.id  = id;
	}

	/**
	 * Sets the children of the term.
	 * 
	 * @param children the children of the term.
	 */
	public void setChildren(OntologyTerm[] children) {
		this.childrenTerm = children;
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioOntologyTerm#getParentTerm()
	 */
	public IBioOntologyTerm getParentTerm() {
		return parentTerm;
	}
	/**
	 * Sets the parent of the term.
	 * @param parent
	 */
	public void setParentTerm(IBioOntologyTerm parentTerm) {
		this.parentTerm = parentTerm;
	}
}
