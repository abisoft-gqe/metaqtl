/*  
 *  src/org/thalia/bio/entity/LocusContainer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import java.util.HashMap;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioLGroup;
import org.metaqtl.bio.IBioLocus;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class LocusContainer extends BioEntityContainer implements IBioLGroup {

	/**
	 * 
	 */
	public LocusContainer() {super();}

	/**
	 * @param name
	 * @param parent
	 */
	public LocusContainer(String name, IBioEntity parent) {
		super(name, parent);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public abstract int getType();

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLGroup#getGenome()
	 */
	public IBioGenome getGenome() {
		return (IBioGenome) parent;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLGroup#setGenome(org.cookqtl.bio.entity.IBioGenome)
	 */
	public void setGenome(IBioGenome genome) {
		parent = genome;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLGroup#loci()
	 */
	public IBioLocus[] loci() {
		IBioEntity[] entities = entities();
		IBioLocus[] loci       = new IBioLocus[entities.length];
		for(int i=0; i<loci.length;i++)
			loci[i] = (IBioLocus) entities[i];
		return loci;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLGroup#addLocus(org.cookqtl.bio.entity.IBioLocus)
	 */
	public void addLocus(IBioLocus locus) {
		addEntity(locus);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLGroup#getLocus(java.lang.String)
	 */
	public IBioLocus getLocus(String name) {
		return (IBioLocus) getEntity(name);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLGroup#removeLocus(java.lang.String)
	 */
	public void removeLocus(String name) {
		removeEntity(name);
	}
	
	public int getLocusNumber() {
		return entityNumber();
	}
	
	public void setLoci(IBioLocus[] loci) {
		if (loci==null) {
			this.entities=null;
			return;
		}
		this.entities = new HashMap(loci.length);
		for(int i=0;i<loci.length;i++) {
			this.entities.put(loci[i].getName(), loci[i]);
		}
	}

}
