/*  
 *  src/org/thalia/bio/entity/factory/XmlBioEntityFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.factory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IMarshallingContext;
import org.jibx.runtime.IUnmarshallingContext;
import org.jibx.runtime.JiBXException;
import org.metaqtl.bio.IBioAdaptable;
import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.entity.BioEntity;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *  
 */
public abstract class XmlBioEntityFactory extends BioEntityFactory {

    protected abstract Class getEntityBeanClass();
    
    protected abstract int getEntityType();

    /*
     * (non-Javadoc)
     * 
     * @see org.cookqtl.core.runtime.ITiedFactory#load(java.io.InputStream)
     */
    public IBioEntity load(InputStream stream) throws IOException {
        try {
            IBindingFactory bfact;
            bfact = BindingDirectory.getFactory(getEntityBeanClass());
            IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
            Object object = uctx.unmarshalDocument(
                    stream, null);
            IBioEntity entity = BioEntity.newBioEntity(getEntityType());
            return entity.getBioAdapter().toEntity(object);            
        } catch (JiBXException e) {
            e.printStackTrace();
            throw new IOException();

        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cookqtl.core.runtime.ITiedFactory#load(java.io.Reader)
     */
    public IBioEntity load(Reader reader) throws IOException {
    	
        IBindingFactory bfact;
        
        try {
        
        	bfact = BindingDirectory.getFactory(getEntityBeanClass());
            IUnmarshallingContext uctx = bfact.createUnmarshallingContext();
            Object object = uctx.unmarshalDocument(reader, null);
            IBioEntity entity = BioEntity.newBioEntity(getEntityType());
            return entity.getBioAdapter().toEntity(object);
            
        } catch (JiBXException e) {
        	  e.printStackTrace();
            throw new IOException();

        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cookqtl.core.runtime.ITiedFactory#unload(java.lang.Object,
     *      java.io.Writer)
     */
    public void unload(IBioEntity obj, OutputStream stream) throws IOException {
        try {
            
        	IBioAdapter adapter;
            
            Object bean = null;
            
            adapter = ((IBioAdaptable) obj).getBioAdapter();
            
            bean = adapter.fromEntity((IBioEntity)obj);
            
            IBindingFactory bfact = BindingDirectory.getFactory(bean.getClass());
            
            IMarshallingContext mctx = bfact.createMarshallingContext();
            
            mctx.setIndent(3);
            mctx.marshalDocument(bean, "UTF-8", null, stream);
            
        } catch (JiBXException e) {
        	
        	  e.printStackTrace();
            throw new IOException();

        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cookqtl.core.runtime.ITiedFactory#unload(java.lang.Object,
     *      java.io.Writer)
     */
    public void unload(IBioEntity obj, Writer writer) throws IOException {
        try {
        	IBioAdapter adapter;
            
            Object bean = null;
            
            adapter = ((IBioAdaptable) obj).getBioAdapter();
            
            bean = adapter.fromEntity((IBioEntity)obj);
            
            IBindingFactory bfact = BindingDirectory.getFactory(bean.getClass());
            
            IMarshallingContext mctx = bfact.createMarshallingContext();
            
            mctx.setIndent(1);
            
            mctx.marshalDocument(bean, "UTF-8", null, writer);
            
        } catch (JiBXException e) {
        	e.printStackTrace();
            throw new IOException(e.getMessage());

        }

    }
    
}