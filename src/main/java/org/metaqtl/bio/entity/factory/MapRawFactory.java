/*  
 *  src/org/thalia/bio/entity/factory/MapRawFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.StringTokenizer;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.entity.GeneticMap;
import org.metaqtl.bio.entity.LGroup;
import org.metaqtl.bio.entity.Marker;
import org.metaqtl.bio.util.LGroupPosition;

/**
 * @author jveyrier
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MapRawFactory extends BioEntityFactory {

	/**
	 * @param inputStream
	 * @return
	 */
	public IBioEntity load(InputStream inputStream) throws IOException {
		InputStreamReader reader;
		BufferedReader buffer;
		String line,name,posStr;
		double pos;
		
		GeneticMap map   = new GeneticMap();
        LGroup     group = null;
        Marker     mrk   = null;
         
        
        reader = new InputStreamReader(inputStream);
        buffer = new BufferedReader(reader);
        
            try {
				while ((line = buffer.readLine()) != null) {
					
					if (line.startsWith("chrom")) {
						/* Means that a new chrom start from here */
						StringTokenizer st = new StringTokenizer(line);
						st.nextToken();
						name  = st.nextToken();
						group = new LGroup(name, map);
						map.addGroup(group);						
					}
					else if (group != null) {
						StringTokenizer st = new StringTokenizer(line);
						name   = st.nextToken();
						posStr = st.nextToken();
						try { 
							pos    = Double.parseDouble(posStr);
						} catch (NumberFormatException e1) {
							/* May be it is due to a coma instead of a decimal
							 * dot separator */
							try {
							   pos	   = Double.parseDouble(posStr.replace(',', '.'));
							} catch (NumberFormatException e2) {
								pos = 0.0;
							}
						}
						mrk = new Marker(name, group);
						mrk.setPosition(LGroupPosition.newStaticLocusPosition(pos));
				        group.addLocus(mrk);				        
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        
        return map;
		
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.extend.entity.factory.BioEntityFactory#load(java.io.Reader)
	 */
	public IBioEntity load(Reader reader) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.extend.entity.factory.BioEntityFactory#unload(org.thalia.bio.entity.IBioEntity, java.io.Writer)
	 */
	public void unload(IBioEntity entity, Writer writer) throws IOException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.extend.entity.factory.BioEntityFactory#unload(org.thalia.bio.entity.IBioEntity, java.io.OutputStream)
	 */
	public void unload(IBioEntity entity, OutputStream stream) throws IOException {
		// TODO Auto-generated method stub
		
	}

}
