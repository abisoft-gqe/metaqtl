/*  
 *  src/org/thalia/bio/entity/factory/BioEntityFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.factory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import org.metaqtl.bio.IBioEntity;
/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class BioEntityFactory  {
	
	public abstract IBioEntity load(Reader reader) throws IOException;
	
	public abstract IBioEntity load(InputStream stream) throws IOException;
	
	public abstract void unload(IBioEntity entity, Writer writer) throws IOException;
	
	public abstract void unload(IBioEntity entity, OutputStream stream) throws IOException;

}
