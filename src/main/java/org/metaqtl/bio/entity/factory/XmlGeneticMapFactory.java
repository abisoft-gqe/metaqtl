/*  
 *  src/org/thalia/bio/entity/factory/XmlGeneticMapFactory.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity.factory;

import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.entity.bean.GeneticMapBean;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *  
 */
public class XmlGeneticMapFactory extends XmlBioEntityFactory {

    /*
     * (non-Javadoc)
     * 
     * @see org.cookqtl.bio.extend.entity.factory.BioEntityFactory#getEntityBeanClass()
     */
    protected Class getEntityBeanClass() {
        return GeneticMapBean.class;
    }

	/* (non-Javadoc)
	 * @see org.thalia.bio.extend.entity.factory.XMLBioEntityFactory#getEntityType()
	 */
	protected int getEntityType() {
		return IBioEntityTypes.GENOME;
	}

}