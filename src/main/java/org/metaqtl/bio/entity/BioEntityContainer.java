/*  
 *  src/org/thalia/bio/entity/BioEntityContainer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import java.util.HashMap;

import org.metaqtl.bio.IBioEntity;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class BioEntityContainer
	extends BioEntity
	implements IBioEntity {

	/**
	 * 
	 */
	public BioEntityContainer() {
		super();
		entities = new HashMap();
	}

	protected HashMap entities;
	
	/**
	 * @param name
	 * @param parent
	 */
	public BioEntityContainer(String name, IBioEntity parent) {
		super(name, parent);
		entities = new HashMap();
	}

	protected void addEntity(IBioEntity entity) {
		entities.put(entity.getName(), entity);		
	}
	
	protected IBioEntity getEntity(String name) {
		return (BioEntity) entities.get(name);
	}
	
	protected int entityNumber() {
		return entities.size();
	}
	
	protected IBioEntity[] entities() {
		if (entities.size() == 0) 
			return (IBioEntity[]) IBioConstants.EMPTY_BIOENTITY_ARRAY;
		IBioEntity[] array = new BioEntity[entities.size()];
		array = (IBioEntity[]) entities.values().toArray(array);
		return array;
	}
	
 	protected void removeEntity(String name) {
 		entities.remove(name);
 	}
}
