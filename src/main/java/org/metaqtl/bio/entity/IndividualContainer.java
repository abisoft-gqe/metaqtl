/*  
 *  src/org/thalia/bio/entity/IndividualContainer.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioIndividual;
import org.metaqtl.bio.IBioPopulation;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public abstract class IndividualContainer
	extends BioEntityContainer
	implements IBioPopulation {

	/**
	 * 
	 */
	public IndividualContainer() {super();}

	/**
	 * @param name
	 * @param parent
	 */
	public IndividualContainer(String name, IBioEntity parent) {
		super(name, parent);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public abstract int getType();	

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioPopulation#getIndividual(java.lang.String)
	 */
	public IBioIndividual getIndividual(String name) {
		return (IBioIndividual) getEntity(name);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioPopulation#addIndividual(org.cookqtl.bio.entity.IBioIndividual)
	 */
	public void addIndividual(IBioIndividual individual) {
		addEntity(individual);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioPopulation#removeIndividual(org.cookqtl.bio.entity.IBioIndividual)
	 */
	public void removeIndividual(String name) {
		removeEntity(name);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioPopulation#individuals()
	 */
	public IBioIndividual[] individuals() {
		return (IBioIndividual[]) entities();
	}

		/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioPopulation#getIndividualNumber()
	 */
	public abstract int getIndividualNumber();	
	
}
