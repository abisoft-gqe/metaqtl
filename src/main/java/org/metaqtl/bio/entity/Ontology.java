/*  
 *  src/org/thalia/bio/entity/Ontology.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.IBioOntology;
import org.metaqtl.bio.IBioOntologyTerm;
import org.metaqtl.bio.entity.adapter.OntologyBeanAdapter;

/**
 * 
 */
public class Ontology extends BioEntity implements IBioOntology{
	
	/**
	 * The function of the ontology.
	 */
	private String function=null;
	/**
	 * The terms of the ontology.
	 */
	private IBioOntologyTerm root=null;
	
	/**
	 * Set the function of the ontology.
	 * @param function
	 */
	public void  setFunction(String function) {
		this.function = function;
	}
	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioOntology#getFunction()
	 */
	public String getFunction() {
		return function;
	}
	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioOntology#getRoot()
	 */
	public IBioOntologyTerm getRoot() {
		return root;
	}
	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioOntology#getTerm(org.thalia.bio.entity.IBioOntologyTerm)
	 */
	public IBioOntologyTerm getTerm(IBioOntologyTerm term) {
		// TODO Auto-generated method stub
		return null;
	}
	/* (non-Javadoc)
	 * @see org.thalia.bio.extend.entity.BioEntity#getType()
	 */
	public int getType() {
		return IBioEntityTypes.ONTOLOGY;
	}
	/* (non-Javadoc)
	 * @see org.thalia.core.runtime.IBeanAdaptable#getBeanAdapter()
	 */
	public IBioAdapter getBioAdapter() {
		return new OntologyBeanAdapter();
	}
	/* (non-Javadoc)
	 * @see org.thalia.bio.IBioOntology#setRoot(org.thalia.bio.IBioOntologyTerm)
	 */
	public void setRoot(IBioOntologyTerm root) {
		this.root = root;
	}
	/**
	 * @param root the root of the ontology.
	 */
	public void setRoot(OntologyTerm root) {
		this.root = root;
	}
	/* (non-Javadoc)
	 * @see org.thalia.bio.IBioOntology#getTerm(java.lang.String)
	 */
	public IBioOntologyTerm getTerm(String termName) {
		return getTerm(root, termName);
	}
	/**
	 * This methods finds the term which names matches the given
	 * one <code>termName</code> from the term <code>root</code>
	 * included.
	 * 
	 * @param root the current root from which the search it's done.
	 * @param termName the name of the term to find.
	 */
	private IBioOntologyTerm getTerm(IBioOntologyTerm root, String termName) {
		if (root == null) return null;
		
		if (root.getName().equals(termName)) return root;
		
		if (root.hasChildren()) {
			IBioOntologyTerm[] children = root.getChildrenTerm();
			for(int i=0;i<children.length;i++) {
				IBioOntologyTerm term = getTerm(children[i], termName);
				if (term != null) return term;
			}
		}
		
		return null;
	}

}
