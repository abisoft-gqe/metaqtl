/*  
 *  src/org/thalia/bio/entity/GeneticMap.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.IBioLGroup;
import org.metaqtl.bio.entity.adapter.GeneticMapBeanAdapter;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class GeneticMap extends GroupContainer {

	/**
	 * 
	 */
	public GeneticMap() {
		super();
	}
	/**
	 * @param name
	 * @param parent
	 */
	public GeneticMap(String name, IBioEntity parent) {
		super(name, parent);
	}
	/**
	 * 
	 * @param name
	 */
	public GeneticMap(String name) {
		super(name, null);
	}

	/**
	 * @deprecated
	   * @param map
	   * @return
	 */
	public GeneticMap merge(GeneticMap map) {
		GeneticMap mapReturn = new GeneticMap(name, parent);

		/*clone this to mapReturn*/
		for (int i = 0; i < this.groups().length; i++) {
			mapReturn.addGroup(this.groups()[i]);
		}
		mapReturn.setProperties(this.getProperties());

		/*add map to mapReturn*/
		IBioLGroup[] groups = map.groups();
		for (int i = 0; i < groups.length; i++) {
			IBioLGroup groupTemp = groups[i];
			IBioLGroup groupNew = mapReturn.getGroup(groupTemp.getName());

			if (groupNew == null) {
				System.out.println(
					"Error group didn't exist in GeneticMap.merge()");
				return null; //TODO
			} else {
				for (int j = 0; j < groupTemp.loci().length; j++) {
					groupNew.addLocus(groupTemp.loci()[j]);
				}
			}
		}

		return mapReturn;
	}
	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public int getType() {
		return IBioEntityTypes.GENOME;
	}
	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdaptable#getBeanAdapter()
	 */
	public IBioAdapter getBioAdapter() {
		return new GeneticMapBeanAdapter();
	}

	
}
