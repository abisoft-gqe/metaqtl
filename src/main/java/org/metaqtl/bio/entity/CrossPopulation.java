/*  
 *  src/org/thalia/bio/entity/CrossPopulation.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioCross;
import org.metaqtl.bio.IBioCrossTypes;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.IBioIndividual;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class CrossPopulation
	extends Population
	implements IBioCross {

	private int type;
	
	/**
	 * 
	 */
	public CrossPopulation() {super();type=IBioCrossTypes.UNKNOWN;}

	/**
	 * @param name
	 * @param parent
	 */
	public CrossPopulation(String name, IBioEntity parent) {
		super(name, parent);
	}

	public CrossPopulation(String name, IBioEntity parent, int type) {
		 super(name, parent);
		 this.type = type;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public int getCrossType() {
		return type;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.extend.entity.IndividualContainer#getType()
	 */
	public int getType() {
		return IBioEntityTypes.POPULATION;
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioCross#getParents()
	 */
	public IBioIndividual[] getParents() {
		// TODO Auto-generated method stub
		return parents;
	}	
    /**
     * @param type The type to set.
     */
    public void setCrossType(int type) {
        this.type = type;
    }

	
}
