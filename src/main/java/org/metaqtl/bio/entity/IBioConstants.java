/*  
 *  src/org/thalia/bio/entity/IBioConstants.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAllele;
import org.metaqtl.bio.IBioLGroup;
import org.metaqtl.bio.IBioLocus;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public interface IBioConstants {

	public static final BioEntity[] EMPTY_BIOENTITY_ARRAY = new BioEntity[0];

	public static final IBioAllele[] EMPTY_BIOALLELE_ARRAY = new IBioAllele[0];

	public static final IBioLGroup[] EMPTY_BIOLGROUP_ARRAY = new IBioLGroup[0]; 
	
	public static final IBioLocus[] EMPTY_BIOLOCUS_ARRAY = new IBioLocus[0];

}
