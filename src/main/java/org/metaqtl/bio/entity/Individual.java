/*  
 *  src/org/thalia/bio/entity/Individual.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioEntityTypes;
import org.metaqtl.bio.IBioIndividual;
import org.metaqtl.bio.IBioIndividualTree;
import org.metaqtl.bio.IBioPopulation;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class Individual extends BioEntityContainer{

	/**
	 * 
	 */
	public Individual() {super();}

	protected IBioPopulation population;
	/**
	 * @param name
	 * @param parent
	 */
	public Individual(String name, IBioEntity parent) {
		super(name, parent);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioEntity#getType()
	 */
	public  int getType() {
		return IBioEntityTypes.INDIVIDUAL;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioIndividual#getPedigree()
	 */
	public IBioIndividualTree getPedigree() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioIndividual#setPedigree(org.cookqtl.bio.entity.IBioIndividualTree)
	 */
	public void setPedigree(IBioIndividualTree pedigree) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioIndividual#getPopulation()
	 */
	public IBioPopulation getPopulation() {
		return population;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioIndividual#setPopulation(org.cookqtl.bio.entity.IBioPopulation)
	 */
	public void setPopulation(IBioPopulation population) {
		this.population =population;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.core.runtime.IBeanAdaptable#getBeanAdapter()
	 */
	public IBioAdapter getBioAdapter() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.thalia.bio.entity.IBioIndividual#getParents()
	 */
	public IBioIndividual[] getParents() {
		// TODO Auto-generated method stub
		return null;
	}

}
