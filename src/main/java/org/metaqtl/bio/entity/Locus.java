/*  
 *  src/org/thalia/bio/entity/Locus.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.entity;

import org.metaqtl.bio.IBioAdapter;
import org.metaqtl.bio.IBioEntity;
import org.metaqtl.bio.IBioLGroup;
import org.metaqtl.bio.IBioLocus;
import org.metaqtl.bio.entity.adapter.LocusBeanAdapter;
import org.metaqtl.bio.util.BioLocusTypes;
import org.metaqtl.bio.util.ILGroupPosition;
import org.metaqtl.bio.util.LGroupPosition;

public abstract class Locus extends AlleleContainer implements IBioLocus {

	/**
	 *  
	 */
	public Locus() {
		super();
	}

	protected ILGroupPosition position;

	/**
	 * @param name
	 * @param parent
	 */
	public Locus(String name, IBioEntity parent) {
		super(name, parent);
	}
	/**
	 * Returns a new Locus
	 * from the given locus type.
	 * @param locusType
	 * @return
	 */
	public static Locus newLocus(int locusType) {
		switch (locusType) {
			case IBioLocus.MARKER :
				return new Marker();
			case IBioLocus.QTL :
				return new QTL();
			default :
				return new Marker();
		}
	}
	/*** Returns a new Locus
	 * from the given locus type.
	
		   * @param string
		   * @return
		   */
	public static Locus newLocus(String string) {
		if (BioLocusTypes.parseType(string)==IBioLocus.MARKER)
			return new Marker();
		else if (BioLocusTypes.parseType(string)==IBioLocus.QTL)
			return new QTL();
		else
			return new Marker();
	}
	/**
	 * 
	 * @return
	 */
	public static IBioAdapter getLocusAdapter() {
		return new LocusBeanAdapter();
	}

	/* (non-Javadoc)
	* @see org.cookqtl.bio.entity.IBioLocus#getGroup()
	*/
	public IBioLGroup getGroup() {
		return (IBioLGroup) parent;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#setGroup(org.cookqtl.bio.entity.IBioLGroup)
	 */
	public void setGroup(IBioLGroup group) {
		parent = group;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#getPosition()
	 */
	public ILGroupPosition getPosition() {
		return position;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.entity.IBioLocus#setPosition(org.cookqtl.bio.entity.ILGroupPosition)
	 */
	public void setPosition(ILGroupPosition position) {
		this.position = position;
	}
	/**
	 * Set the position of the locus on the linkage group in
	 * static mode i.e without uncertainty.
	 * @param position the locus position - absolute. 
	 */
	public void setPosition(double position) {
		this.position = LGroupPosition.newStaticLocusPosition(position);
	}
	/**
	 * Set the position of the locus on the linkage group in
	 * estimate mode i.e with uncertainty.
	 * @param position the locus position - absolute. 
	 */
	public void setPosition(double position, double sd) {
		this.position = LGroupPosition.newEstimatedLocusPosition(position, sd);
	}	

}
