/*  
 *  src/org/thalia/bio/IBioEntityTypes.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public interface IBioEntityTypes {

	

	/**
		 * The integer code for an individual entity
		 */
		public static final int INDIVIDUAL = 1;

		/**
		 * The integer code for Population entity
		 */
		public static final int POPULATION = 2;

		/**
		 * The integer code for Genome entity
		 */
		public static final int GENOME = 3;

		/**
		 * The integer code for Linkage Group entity
		 */
		public static final int LGROUP = 4;

		/**
		 * The integer code for Locus entity
		 */
		public static final int LOCUS = 5;
		
		public static final int MARKERLOCUS = 6;
		
		public static final int QTLLOCUS = 7;	

		/**
		 * The integer code for Allele entity
		 */
		public static final int ALLELE = 8;
		
	    public static final int COLLECTION = 9;
	    
	    public static final int ONTOLOGY_TERM = 10;

		public static final int ONTOLOGY = 11;


}
