/*  
 *  src/org/thalia/bio/util/BioEntityNameComparator.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.util;

import java.util.Comparator;

import org.metaqtl.bio.IBioEntity;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class BioEntityNameComparator implements Comparator {

	private static BioEntityNameComparator compare;
		
		private BioEntityNameComparator() {
			compare = this;
		}
		
		public static BioEntityNameComparator getInstance(){
			if (compare == null)
				new BioEntityNameComparator();
			return compare;
		}
		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			if (IBioEntity.class.isAssignableFrom(o1.getClass()) && IBioEntity.class.isAssignableFrom(o2.getClass())) {
				
				if (o1.getClass().getName().compareTo(o1.getClass().getName()) == 0) {
					// The comparison is based on the name and the class of the entities.
					if (((IBioEntity)o1).getName() == null  && ((IBioEntity)o2).getName() == null)
						return 0;
					else
						return ((IBioEntity)o1).getName().compareTo(((IBioEntity)o2).getName());
				}
				else return -1;
			}
			return -1;
		}
}
