/*  
 *  src/org/thalia/bio/util/LocusPositionComparator.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.util;

import java.util.Comparator;

import org.metaqtl.bio.IBioLocus;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public class LocusPositionComparator implements Comparator {

		private static LocusPositionComparator instance;
		
		private LocusPositionComparator() {
			instance = this;
		}
		
		public static LocusPositionComparator getInstance() {
			if (instance == null)
				 new LocusPositionComparator();
			return instance;			
		}
		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			IBioLocus l1 = (IBioLocus) o1;
			IBioLocus l2 = (IBioLocus) o2;
			double diff = l1.getPosition().absolute()-l2.getPosition().absolute();
			if (java.lang.Math.abs(diff) >= 1)
				return (int) diff;
			else if (diff < 0)
				return -1;
			else 
				return 1;
		}
	


}
