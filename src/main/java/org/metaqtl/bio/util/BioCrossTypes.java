/*  
 *  src/org/thalia/bio/util/BioCrossTypes.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.util;

import org.metaqtl.bio.IBioCrossTypes;

/**
 * @author jveyrier
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public final class BioCrossTypes {
	
	public static int type;
	
	public static int gen;
	
	/**
	 * Parse the given string and return if valid the type of
	 * cross and the number of generations.
	 * 
	 * Design									Code 		Example
	 * 
	 * Backcross j times to Pi 						BCi			BC1
	 * Selfed Generation i intercross 				SFi			SF3
	 * Randomly mated generation i intercross		RFi			RF2
	 * Double Haploid								RI0			RI0
	 * RIL via selfing								RI1			RI1
	 * RIL via sib-mating							RI2			RI2
	 * IRI via selfing + i randomly mated gen	    IRI0i		IRI04
	 * IRI via sib-mating + i randomly mated gen    IRI1i		IRI13
	 * 
	 * @param str
	 */
	public static void parseCross(String str) {
		
		type = IBioCrossTypes.UNKNOWN;
		gen  = 0;
		
		switch(str.charAt(0)) {
			case 'B' : /* Backcross "BC" */ 
				type = IBioCrossTypes.BC;
				if (str.length() >= 3)
					gen  = Integer.parseInt(str.substring(2));
				break;
			case 'S' : /* Sefled geenration i intercross */
				type = IBioCrossTypes.SF;
				if (str.length() >= 3)
					gen  = Integer.parseInt(str.substring(2));
				break;
			case 'R' :
				switch(str.charAt(1)) {
					case 'F' : /* Randomly mated generation */
						type = IBioCrossTypes.RF;
						if (str.length() >= 3)
						   gen  = Integer.parseInt(str.substring(2));
						break;
					case 'I' : /* Recombinant Inbred*/
						switch(str.charAt(2)) {
							case '0' : /* Double Haploid */
								type = IBioCrossTypes.DH;
								gen  = 1;
								break;
							case '1' :	/* selfing */
								type = IBioCrossTypes.RILSe;
								gen  = 1;
								break;
 							case '2' :  /* ran mated */
 								type = IBioCrossTypes.RILSi;
 								gen  = 1;
 								break;
						}
						break;
				}
				break;
			case 'I' :	/* High Recombinant Inbred Lines IRI */
				switch(str.charAt(3)) {
					case '0' :
						/* IRI0rg = selfing */
						type = IBioCrossTypes.IRISe;
						if (str.length() >= 5)
							gen  = Integer.parseInt(str.substring(4));
						break;
					case '1' :
						/* IRI1rg = ran mated */
						type = IBioCrossTypes.IRISi;
						if (str.length() >= 5)
							gen  = Integer.parseInt(str.substring(4));
						break;
				}
				break;
		}
    }

	/**
	 * @param type2
	 * @param gen2
	 * @return
	 */
	public static String crossToString(int type1, int gen1) {
		String str = null;
		
		switch(type1) {
			case IBioCrossTypes.BC :
				str="BC"+gen1;
				break;
			case IBioCrossTypes.DH :
				str="RI0";
				break;
			case IBioCrossTypes.IRISe :
				str="IRI0"+gen1;
				break;
			case IBioCrossTypes.IRISi :
				str="IRI1"+gen1;
				break;
			case IBioCrossTypes.RF :
				str="RF"+gen1;
				break;
			case IBioCrossTypes.RILSe :
				str="RI1";
				break;
			case IBioCrossTypes.RILSi :
				str="RI2";
				break;
			case IBioCrossTypes.SF :
				str="SF"+gen1;
				break;
			default :
				break;
		}
		
		return str;
	}
	
}
