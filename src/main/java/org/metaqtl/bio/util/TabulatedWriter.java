/*  
 *  src/org/thalia/bio/util/TabulatedWriter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Hashtable;

/**
 * General comments must be added here.
 * 
 * @author Jean-Baptiste
 * 
 * Additions must be added here.
 *  
 */
public class TabulatedWriter {

    private PrintWriter printWriter;

    private String[] keys;

    private String token = "\t";

    public TabulatedWriter(OutputStream stream) {
        printWriter = new PrintWriter(new OutputStreamWriter(stream));
    }

    public TabulatedWriter(Writer writer) {
        printWriter = new PrintWriter(writer);
    }

    /**
     * @return Returns the token.
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token
     *            The token to set.
     */
    public void setToken(String token) {
        this.token = token;
    }

    public void setKeys(String[] keys) {
        this.keys = keys;
        int i;
        for (i = 0; i < keys.length - 1; i++) {
            printWriter.print(keys[i] + token);
        }
        printWriter.print(keys[i]);
        printWriter.println();
    }

    /**
     * Prints one line of tabulated values given the hashtable which links
     * values and keys.
     * 
     * @param values
     */
    public void nextValues(Hashtable values) {
        int i;
        for (i = 0; i < keys.length - 1; i++) {
            printWriter.print(values.get(keys[i]) + "\t");
        }
        printWriter.print(values.get(keys[i]));
        printWriter.println();
        printWriter.flush();
    }

    /**
     * Print one line of tabulated values by assuming that they are in the same
     * order than the keys.
     * 
     * @param values
     */
    public void nextValues(String[] values) {
        int i;
        for (i = 0; i < keys.length - 1; i++) {
            printWriter.print(values[i] + "\t");
        }
        printWriter.print(values[i]);
        printWriter.println();
        printWriter.flush();
    }

    public void close() throws IOException {
        printWriter.close();
    }

    /**
     *  
     */
    public int size() {
        return keys.length;
    }
}