/*  
 *  src/org/thalia/bio/util/BioUtilities.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.util;

import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.IBioLGroup;
import org.metaqtl.bio.IBioLocus;

/**
   * General comments must be added here.
   *
   * @author Julien Cornouiller
   *
   * Additions must be added here.
   *
   */
public class BioUtilities {
	/**
			 * Sort tab with loci
			   * @param loci
			 */
		public static IBioLocus[] sortLociByPosition(IBioLocus[] loci) {
			int min;
			IBioLocus locus;
			int N = loci.length;

			for (int i = 0; i < N - 1; ++i) {
				min = i;
				for (int j = i + 1; j < N; ++j)
					if (loci[j].getPosition().absolute()
						< loci[min].getPosition().absolute())
						min = j;

				locus = loci[min];

				loci[min] = loci[i];

				loci[i] = locus;

			}

			return loci;

		}
		/**
		 * This method merges two genomes in a single one : <code>g1</code> is
		 * modified and will contain the result of the merge.
		 */
		public static void mergeGenome(IBioGenome g1, IBioGenome g2) {
			
			IBioLGroup[] gg1 = g1.groups();
			IBioLGroup[] gg2 = g2.groups();
			
			for(int i=0;i<gg2.length;i++) {
				
				/* Find the same group in the marker map */
				
				for(int j=0;j<gg1.length;j++) {
					
					if (gg1[j].getName().equals(gg2[i].getName())) {
						
						IBioLocus[] lg2 = gg2[i].loci();
						
						for(int k=0;k<lg2.length;k++) {
							
							gg1[j].addLocus(lg2[k]);
							
						}
						
					}
					
				}
				
			}
		}
}
