/*  
 *  src/org/thalia/bio/util/BioLocusTypes.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.util;

import java.util.ArrayList;

import org.metaqtl.bio.IBioLocus;

/**
 * @author jveyrier
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public final class BioLocusTypes {
	
	public static int parseType(String type) {
		switch(type.charAt(0)) {
			case 'M' :
				return IBioLocus.MARKER;
			case 'Q' :
				return IBioLocus.QTL;
			default :
				return IBioLocus.MARKER;
		}
	}
	
	public static String typeToString(int type) {
		switch(type) {
			case IBioLocus.MARKER :
				return "M";
			case IBioLocus.QTL :
				return "Q";
			default :
				return "M";
		}
	}
	/**
	 * This returns extracts in the given array of loci
	 * the locus which type matches the given one.  
	 * @param loci
	 * @param type
	 * @return
	 */
	public static IBioLocus[] extractLocus(IBioLocus[] loci, int type) {
		int i;
		ArrayList   extList=null;
		IBioLocus[] ext=null;
		
		if (loci==null) return ext;
		
		for(i=0;i<loci.length;i++) {
			if (loci[i].getLocusType()==type) {
				if (extList==null) extList = new ArrayList();
				extList.add(loci[i]);
			}
		}
		
		if (extList != null) {
			ext = new IBioLocus[extList.size()];
			extList.toArray(ext);
		}
		
		return ext;
	}

}
