/*  
 *  src/org/thalia/bio/util/LGroupPosition.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio.util;

/**
 * 
 * Class Description Here
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public  abstract class LGroupPosition implements ILGroupPosition{

	protected double absolute = 0.0;
	
	protected double stderr   = 0.0;
	
	/**
	* @param d
	*/
	public LGroupPosition(double d) {
		absolute = d;
	}
	
	public LGroupPosition(double absolute, double stderr) {
		this.absolute = absolute;
		this.stderr = stderr;
	}
	
	public static LGroupPosition newStaticLocusPosition(double value) {
			return new StaticLocusPosition(value);
	}
	
	public static LGroupPosition newEstimatedLocusPosition(double v, double s) {
		return new EstimatedLocusPosition(v, s);
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.util.ILGroupPosition#absolute()
	 */
	public double absolute() {
		return absolute;
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.util.ILGroupPosition#standardError()
	 */
	public double standardError() {
		return (getType() != ILGroupPosition.STATIC) ? stderr : 0.0;	
	}

	/* (non-Javadoc)
	 * @see org.cookqtl.bio.util.ILGroupPosition#getType()
	 */
	public abstract int getType();
	
	
}
