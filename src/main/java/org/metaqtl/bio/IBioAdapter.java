/*  
 *  src/org/thalia/bio/IBioAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio;

/**
 * 
 */
public interface IBioAdapter {
	/**
	 * Convert the given bio entity object into
	 * another compliant object.
	 * @param obj
	 * @return
	 */
	public IBioEntity toEntity(Object obj);
	/**
	 * Convert the given compliant object to a
	 * bio entity object.
	 * @param obj
	 * @return
	 */
	public Object fromEntity(IBioEntity obj);
}
