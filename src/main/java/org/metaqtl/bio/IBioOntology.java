/*  
 *  src/org/thalia/bio/IBioOntology.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio;

/**
 * This interface defines some rules to handle a ontology.
 * 
 * Here we assume that the onotlogy can be represented by a simple
 * directed acyclic graph, i.e a parent-child linking between the
 * concepts (if A is child of B then B can't be a child of A). 
 */
public interface IBioOntology extends IBioEntity {
	/**
	 * Returns the function of the ontology
	 * @return
	 */
	public String getFunction();
	/**
	 * Returns the root of the ontology tree.
	 * 
	 * @return the root of the ontology tree.
	 */
	public IBioOntologyTerm getRoot();
	/**
	 * Sets the root of the ontology.
	 * 
	 * @param root the root of the ontology
	 */
	public void setRoot(IBioOntologyTerm root);
	/**
	 * Returns if exists the term in the ontology which 
	 * matches the given one.
	 * 
	 * @param term the term to look for in the ontology.
	 * @return the term if found, null otherwise.
	 */
	public IBioOntologyTerm getTerm(IBioOntologyTerm term);
	/**
	 * This method looks into the ontology if there is 
	 * a term with the given name and returns it.
	 * 
	 * @param termName the name of the term to find.
	 * @return the term or null if not found.
	 */
	public IBioOntologyTerm getTerm(String termName);
}
