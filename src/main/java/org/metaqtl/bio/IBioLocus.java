/*  
 *  src/org/thalia/bio/IBioLocus.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.bio;

import org.metaqtl.bio.util.ILGroupPosition;


/**
 * @author Jean-Baptiste Veyrieras 
 *
 */
public interface IBioLocus  extends IBioEntity {
	
	public final int MARKER = 0;
	
	public final int QTL = 1;
	
	public final String[] TypesString = {"M", "Q"};	
	
	public IBioAllele[] alleles();
	
	public void addAllele(IBioAllele allele);
	
	public void removeAllele(String name);
	
	public IBioAllele getAllele(String name);
	
	public IBioLGroup getGroup();
	
	public void setGroup(IBioLGroup group);
	
	public int getAlleleNumber();
	
	public ILGroupPosition getPosition();	
	
	public void setPosition(ILGroupPosition position);
	
	public void setPosition(double position);
	
	public void setPosition(double position, double sd);
	
	public int getType();
	
	public int getLocusType();
}
