/*  
 *  src/org/metaqtl/QtlPartition.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * 
 */
public class QtlPartition {
	
	public static final int HORIZONTAL_MODE = 0;
	
	public static final int VERTICAL_MODE   = 1;
	
	/**
	 * The number of Qtl.
	 */
	public int nqtl;
	/**
	 * The number of partition.
	 */
	public int np=0;
	/**
	 * The partition names.
	 */
	public String[] pnames=null;
	/**
	 * The qtl names
	 */
	public String[] qtlNames=null;
	/**
	 * The probabilities of each Qtl to
	 * belong to each partition.
	 */
	public double[][] proba=null;
	/**
	 * The Qtl positions
	 */
	public double[] qtlPos;
	/**
	 * The Qtl confidence intervals.
	 */
	public double[] qtlCI;
	/**
	 * The Qtl tree.
	 */
	public Tree tree;
	/**
	 * The mode of representation
	 */
	public int mode;
	
	/**
	 * @return
	 */
	public ArrayList getPartNames() {
		if (pnames != null) {
			ArrayList t = new ArrayList(pnames.length);
			for(int i=0;i<pnames.length;i++) {
				if (pnames[i] != null)
					t.add(new String(pnames[i]));
				else
					t.add(new String("QTLPART_" + (i+1)));
			}
			return t;
		} else return new ArrayList();
	}
	/**
	 * @param tree
	 */
	public void setTree(Tree tree) {
		this.tree = tree;
	}
	/**
	 * @param names
	 */
	public void rebuildPartition(String[] pnames) {
		Hashtable names = new Hashtable();
		for(int i=0;i<pnames.length;i++) {
			for(int j=0;j<this.pnames.length;j++) {
				if (this.pnames[j].equals(pnames[i])) {
					names.put(pnames[i], new Integer(i));
				}
			}
		}
		if (names.keySet().size() > 0) {
			double[][] tmp = new double[nqtl][pnames.length];
			for(int i=0;i<nqtl;i++) {
				for(int j=0;j<this.pnames.length;j++) {
					Integer idx = (Integer) names.get(this.pnames[j]);
					tmp[i][idx.intValue()] = proba[i][j];
				}
			}
			np    = pnames.length;
			proba = tmp;
			this.pnames = new String[np];
			for(int j=0;j<this.pnames.length;j++)
				this.pnames[j]=new String(pnames[j]);
		} else {
			proba=null;
		}
	}
	
}
