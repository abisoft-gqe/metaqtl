/*  
 *  src/org/metaqtl/adapter/MetaQtlAnalysisAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.adapter;

import org.metaqtl.Chromosome;
import org.metaqtl.MetaQtlAnalysis;

/**
 * 
 */
public final class MetaQtlAnalysisAdapter {
	/**
	 * 
	 * @param chromosomes
	 * @return
	 */
	public static MetaQtlAnalysis adapt(Chromosome[] chromosomes) {
		
		MetaQtlAnalysis metaQtlAnalysis = null;
		
		if (chromosomes != null) {
			int i,nchr;
			
			for(nchr=i=0;i<chromosomes.length;i++)
				if (chromosomes[i].hasQTL() && chromosomes[i].metaQtlAnalysis!=null) 
					nchr++;
			
			metaQtlAnalysis = new MetaQtlAnalysis(nchr);
			
			for(nchr=i=0;i<chromosomes.length;i++) {
				
				if (chromosomes[i].hasQTL() && chromosomes[i].metaQtlAnalysis!=null) {
					
					metaQtlAnalysis.chromNames[nchr]    = chromosomes[i].name;
					
					metaQtlAnalysis.resultByChrom[nchr] = chromosomes[i].metaQtlAnalysis;
					
					metaQtlAnalysis.qtlByChrom[nchr]    = chromosomes[i].qtls;
					
					nchr++;	
				}
			}
			
		}
		
		return metaQtlAnalysis;
		
	}

}
