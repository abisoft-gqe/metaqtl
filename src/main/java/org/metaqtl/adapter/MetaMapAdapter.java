/*  
 *  src/org/metaqtl/adapter/MetaMapAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.adapter;

import java.util.Properties;

import org.metaqtl.IBioGenomeProperties;
import org.metaqtl.IBioLGroupProperties;
import org.metaqtl.IBioLocusProperties;
import org.metaqtl.MetaChrom;
import org.metaqtl.MetaMap;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.entity.GeneticMap;
import org.metaqtl.bio.entity.LGroup;
import org.metaqtl.bio.entity.Marker;

/**
 * This class defines some usefull methods to adapt {@link MetaMap}
 * objects into other kinds of objects or the inverse.
 */
public final class MetaMapAdapter {

	/**
	 * This methos convert a <code>MetaMap</code> object into
	 * an object which is consistent with the <code>IBioGenome</code>
	 * interface.
	 * @param metaMap the <code>MetaMap</code> to convert.
	 * @return an object which class implements the <code>IBioGenome</code>
	 * interface.
	 */
	public static IBioGenome toIBioGenome(MetaMap metaMap) {
		int i,j;
		MetaChrom metaGroup;
		LGroup group;
		Marker marker;
		GeneticMap geneticMap = null;
		Properties properties;
		
		
		if (metaMap==null) return null;
		
		geneticMap = new GeneticMap();
		properties = geneticMap.getProperties();
		
		properties.setProperty(IBioGenomeProperties.MAPPING_FUNCTION, IBioGenomeProperties.MAPPING_FUNCTION_HALDANE);
		properties.setProperty(IBioGenomeProperties.MAPPING_UNIT, IBioGenomeProperties.MAPPING_UNIT_CM);
		
		geneticMap.setProperties(properties);
		
		for(i=0;i<metaMap.nchr;i++) {
			
			metaGroup = metaMap.chromosomes[i]; 
			
			if (metaMap.chromosomes[i] == null) continue;
			
			group = new LGroup(metaGroup.name, geneticMap);
			
			for(j=0;j<metaGroup.nm;j++) {
				
				marker = new Marker(metaGroup.mrkNames[j], group);
				
				/* As distance are in Morgan in the chromCluster we 
				 * multiply it by 100.
				 */
				
				marker.setPosition(100.0*metaGroup.d[j], 100.0*metaGroup.sdd[j]);
				
				/* Add properties to the marker */
				properties = marker.getProperties();
				properties.setProperty(IBioLocusProperties.OCCURENCE, ""+metaGroup.mrkOc[j]);
				marker.setProperties(properties);
				
				group.addLocus(marker);
				
			}
			
			/* Then add properties to the linkage group */
			properties = group.getProperties();
			
			properties.setProperty(IBioLGroupProperties.CHISQUARE, ""+metaGroup.chi2);
			properties.setProperty(IBioLGroupProperties.PVALUE, ""+metaGroup.pvalue);
			properties.setProperty(IBioLGroupProperties.DOF, ""+metaGroup.dof);
			
			group.setProperties(properties);
			
			geneticMap.addGroup(group);
			
		}
		
		return geneticMap;
	}
	
}
