/*  
 *  src/org/metaqtl/adapter/QtlAdapter.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.adapter;

import java.util.Properties;

import org.metaqtl.EMResult;
import org.metaqtl.IBioLocusProperties;
import org.metaqtl.Qtl;
import org.metaqtl.bio.IBioCrossTypes;
import org.metaqtl.bio.IBioLocus;
import org.metaqtl.bio.entity.Locus;
import org.metaqtl.bio.util.BioCrossTypes;
import org.metaqtl.bio.util.NumberFormat;
/**
 * This class defines some usefull methods to adapt {@link Qtl}
 * object into other kinds of objects or the inverse.
 */
public final class QtlAdapter {
	
	/**
	 * This method converts a {@link Qtl} into an object which class
	 * implements the <code>IBioLocus<code> interface.
	 * 
	 * @param qtl the <code>Qtl</code> to adapt.
	 * @return an object which class implements the <code>IBioLocus<code> interface.
	 */
	public static IBioLocus fromQTL(Qtl qtl) {
		String val;
		Properties properties;
		IBioLocus locus = null;
		
		if (qtl==null) return locus;
		
		locus = Locus.newLocus(IBioLocus.QTL);
		
		locus.setName(qtl.name);
		locus.setPosition(qtl.position);
		
		properties = locus.getProperties();
		
		if (qtl.trait != null) {
			if (qtl.trait.name != null)
				properties.setProperty(IBioLocusProperties.QTL_TRAIT_NAME, qtl.trait.name);
			if (qtl.trait.unit != null)
				properties.setProperty(IBioLocusProperties.QTL_TRAIT_UNIT, qtl.trait.unit);
		}
		if (qtl.cross != null) {
			if (qtl.cross.name != null)
				properties.setProperty(IBioLocusProperties.QTL_CROSS_NAME, qtl.cross.name);
			if (qtl.cross.type != IBioCrossTypes.UNKNOWN) {
				val = BioCrossTypes.crossToString(qtl.cross.type, qtl.cross.gen);
				if (val != null)
					properties.setProperty(IBioLocusProperties.QTL_CROSS_TYPE, val);
			}
			if (qtl.cross.size > 0)
				properties.setProperty(IBioLocusProperties.QTL_CROSS_SIZE, ""+qtl.cross.size);
		}
		if (qtl.ci != null) {
			properties.setProperty(IBioLocusProperties.QTL_CI_FROM, ""+qtl.ci.from);
			properties.setProperty(IBioLocusProperties.QTL_CI_TO,   ""+qtl.ci.to);
			if (qtl.ci.lodDecrease>0.0)
				properties.setProperty(IBioLocusProperties.QTL_CI_LOD_DECREASE, ""+qtl.ci.lodDecrease);
		}
		if (qtl.stats != null) {
			if (qtl.stats.additive!=0.0)
				properties.setProperty(IBioLocusProperties.QTL_STATS_ADDITIVE,""+qtl.stats.additive);
			if (qtl.stats.dominance!=0.0)
				properties.setProperty(IBioLocusProperties.QTL_STATS_DOMINANCE,""+qtl.stats.dominance);
			if (qtl.stats.rsquare!=0.0)
				properties.setProperty(IBioLocusProperties.QTL_STATS_RSQUARE,""+qtl.stats.rsquare);
		}
		if (qtl.proj != null) {
			properties.setProperty(IBioLocusProperties.QTL_PROJ_ORIGIN,""+qtl.proj.mapOrigin);
			properties.setProperty(IBioLocusProperties.QTL_PROJ_INTSCALE,""+qtl.proj.intScale);
			properties.setProperty(IBioLocusProperties.QTL_PROJ_MAPSCALE,""+qtl.proj.mapScale);
			properties.setProperty(IBioLocusProperties.QTL_PROJ_SHAREFLANKING,""+qtl.proj.shareFlanking);
			properties.setProperty(IBioLocusProperties.QTL_PROJ_SWAPFLANKING,""+qtl.proj.swap);
		}		
		
		locus.setProperties(properties);
		
		return locus;
	}
	/**
	 * This method converts an object which class
	 * implements the <code>IBioLocus<code> interface
	 * into a {@link Qtl}.
	 * 
	 * @param an object which class implements the <code>IBioLocus<code> interface.
	 * @return a <code>Qtl</code>
	 */
	public static Qtl toQTL(IBioLocus locus) {
		double d;
		String value;
		Properties properties;
		Qtl qtl = null;
		
		if (locus==null) return null;
		
		qtl = new Qtl();
		
		qtl.position = locus.getPosition().absolute();
		
		/* Copy the name of the locus */
		qtl.name = locus.getName();
		/* now copy properties */
		properties = locus.getProperties();
		/* Trait */
		value = properties.getProperty(IBioLocusProperties.QTL_TRAIT_NAME);
		if (value != null && !value.equals("")) {
			qtl.trait	   = qtl.new QTLTrait();
			qtl.trait.name = value;
			/* Trait unit */
			value = properties.getProperty(IBioLocusProperties.QTL_TRAIT_UNIT);
			if (value != null && !value.equals("")) {
				qtl.trait.unit = value;
			}
			/* Trait group */
			value = properties.getProperty(IBioLocusProperties.QTL_TRAIT_GROUP);
			if (value != null && !value.equals("")) {
				qtl.trait.group = value;
			}
		}
		/* Stats */
		value = properties.getProperty(IBioLocusProperties.QTL_STATS_RSQUARE);
		if (value != null && !value.equals("")) {
			d = NumberFormat.parseDouble(value);
			if (qtl.stats == null) qtl.stats = qtl.new QTLStats();
			qtl.stats.rsquare = NumberFormat.parseDouble(value);
		}
		value = properties.getProperty(IBioLocusProperties.QTL_STATS_ADDITIVE);
		if (value != null && !value.equals("")) {
			if (qtl.stats == null) qtl.stats = qtl.new QTLStats();
			qtl.stats.additive = NumberFormat.parseDouble(value);
		}
		value = properties.getProperty(IBioLocusProperties.QTL_STATS_DOMINANCE);
		if (value != null && !value.equals("")) {
			if (qtl.stats == null) qtl.stats = qtl.new QTLStats();
			qtl.stats.dominance = NumberFormat.parseDouble(value);
		}
		/* Confidence interval */
		value = properties.getProperty(IBioLocusProperties.QTL_CI_FROM);
		if (value != null && !value.equals("")) {
			if (qtl.ci == null) qtl.ci = qtl.new QTLSupport();
			qtl.ci.from = NumberFormat.parseDouble(value);
		} 
		value = properties.getProperty(IBioLocusProperties.QTL_CI_TO);
		if (value != null && !value.equals("")) {
			if (qtl.ci == null) qtl.ci = qtl.new QTLSupport();
			qtl.ci.to = NumberFormat.parseDouble(value);
		}
		value = properties.getProperty(IBioLocusProperties.QTL_CI_LOD_DECREASE);
		if (value != null && !value.equals("")) {
			if (qtl.ci == null) qtl.ci = qtl.new QTLSupport();
			qtl.ci.lodDecrease = NumberFormat.parseDouble(value);
		}
		if (qtl.ci != null) {
			if ((qtl.ci.to == qtl.ci.from) && (qtl.ci.from == 0.0)) {
				/* This means that the CI is undefined */
				qtl.ci = null;
			}
		}
		/* Cross design */
		value = properties.getProperty(IBioLocusProperties.QTL_CROSS_NAME);
		if (value != null  && !value.equals("")) {
			if (qtl.cross == null) qtl.cross = qtl.new QTLCross();
			qtl.cross.name = value;
		}
		value = properties.getProperty(IBioLocusProperties.QTL_CROSS_TYPE);
		if (value != null  && !value.equals("")) {
			if (qtl.cross == null) qtl.cross = qtl.new QTLCross();
			BioCrossTypes.parseCross(value);
			qtl.cross.type = BioCrossTypes.type;
			qtl.cross.gen  = BioCrossTypes.gen;
		}
		value = properties.getProperty(IBioLocusProperties.QTL_CROSS_SIZE);
		if (value != null  && !value.equals("")) {
			if (qtl.cross == null) qtl.cross = qtl.new QTLCross();
			try {
			   qtl.cross.size = Integer.parseInt(value);
			} catch (NumberFormatException e) {
			   qtl.cross.size = 0;
			}
		}
		
		/* Projection */
		value = properties.getProperty(IBioLocusProperties.QTL_PROJ_INTSCALE);
		if (value != null  && !value.equals("")) {
			if (qtl.proj == null) qtl.proj = qtl.new QTLProj();
			qtl.proj.intScale = NumberFormat.parseDouble(value);
		}
		value = properties.getProperty(IBioLocusProperties.QTL_PROJ_MAPSCALE);
		if (value != null  && !value.equals("")) {
			if (qtl.proj == null) qtl.proj = qtl.new QTLProj();
			qtl.proj.mapScale = NumberFormat.parseDouble(value);
		}
		value = properties.getProperty(IBioLocusProperties.QTL_PROJ_SHAREFLANKING);
		if (value != null  && !value.equals("")) {
			if (qtl.proj == null) qtl.proj = qtl.new QTLProj();
			qtl.proj.shareFlanking = Boolean.parseBoolean(value);
		}
		value = properties.getProperty(IBioLocusProperties.QTL_PROJ_SWAPFLANKING);
		if (value != null  && !value.equals("")) {
			if (qtl.proj == null) qtl.proj = qtl.new QTLProj();
			qtl.proj.swap = Boolean.parseBoolean(value);
		}
		
		return qtl;
	}
	/**
	 * From a result of a EM clustering returns the 
	 * mixture components as an array of Qtl.
	 * @param result
	 * @return
	 */
	public static Qtl[] adapt(EMResult result) {
		if (result == null) return null;
		
		Qtl[] qtls = null;
		
		qtls = new Qtl[result.k];
		
		for(int i=0;i<result.k;i++) {
			qtls[i] = new Qtl();
			qtls[i].position    = result.mu[i];
			qtls[i].sd_position = Math.sqrt(result.ocov[i][i]);
			qtls[i].ci 			= qtls[i].new QTLSupport();
			qtls[i].ci.from 	= result.mu[i]-qtls[i].sd_position*1.96;
			qtls[i].ci.to   	= result.mu[i]+qtls[i].sd_position*1.96;
		}
		
		return qtls;
	}
}
