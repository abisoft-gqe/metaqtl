/*  
 *  src/org/metaqtl/util/MappingFunction.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.util;

import org.metaqtl.IMetaQtlConstants;

/**
 * 
 * define all cartogarphies functions
 * 
 * @author Jean-Baptiste Veyrieras
 *
 */
public final strictfp class MappingFunction {

	/**
	 * Returns the recombination rate from the distance.
	 * unit :
	 * 	0 -> M
	 * 	1 -> cM
	 * @param distance
	 */
	public static double recombination(
		double distance,
		int function,
		int unit) {
		double scale = 1;
		if (unit == IMetaQtlConstants.MAPPING_UNIT_CM)
			scale = 100;
		if (function == IMetaQtlConstants.MAPPING_FUNCTION_HALDANE) {
			return 0.5 * (1 - java.lang.Math.exp(-2 * distance / scale));
		} else if (function == IMetaQtlConstants.MAPPING_FUNCTION_KOSAMBI) {
			return (1-java.lang.Math.exp(-4*distance/scale))/(2*(1+java.lang.Math.exp(-4*distance/scale)));
		}
		return -1;
	}

	/**
	 * Returns the distance from the recombination rate.
	 * unit :
	 * 	0 -> M
	 * 	1 -> cM
	 * @param recombination
	 */
	public static double distance(
		double recombination,
		int function,
		int unit) {
		double scale = 1.0;
		if (unit == IMetaQtlConstants.MAPPING_UNIT_CM)
			scale = 100;
		if (function == IMetaQtlConstants.MAPPING_FUNCTION_HALDANE) {
			return -0.5 * scale * java.lang.Math.log(1 - 2 * recombination);
		} else if (function == IMetaQtlConstants.MAPPING_FUNCTION_KOSAMBI) {
			return 0.25 * scale * (java.lang.Math.log(1 + 2 * recombination)-java.lang.Math.log(1-2*recombination));
		}
		return -1;
	}
	/**
	 * Returns the variance of the distance for a given recombination
	 * rate r, its variance and the mapping function.
	 * unit :
	 * 	0 -> M
	 * 	1 -> cM
	 * @param r
	 * @param rvar
	 * @param function
	 * @return
	 */
	public static double varianceDistance(
		double r,
		double rvar,
		int function,
		int unit) {
		double scale = 1.0;
		if (unit == IMetaQtlConstants.MAPPING_UNIT_CM)
			scale = 100;
		if (function == IMetaQtlConstants.MAPPING_FUNCTION_HALDANE) {
			return rvar * scale * scale / ((1.0 - 2.0 * r) * (1.0 - 2.0 * r));
		} else if (function == IMetaQtlConstants.MAPPING_FUNCTION_KOSAMBI) {
			return rvar * scale * scale /((1.0-4.0*r*r)*(1.0-4.0*r*r));
		}
		return 1;		
	}
}
