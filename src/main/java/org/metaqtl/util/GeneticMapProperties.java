/*  
 *  src/org/metaqtl/util/GeneticMapProperties.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.util;

import java.util.Properties;

import org.metaqtl.IBioGenomeProperties;
import org.metaqtl.IMetaQtlConstants;
import org.metaqtl.bio.IBioCrossTypes;
import org.metaqtl.bio.IBioGenome;
import org.metaqtl.bio.entity.CrossPopulation;
import org.metaqtl.bio.util.BioCrossTypes;

/**
 * General comments must be added here.
 * 
 * @author Julien Cornouiller
 * 
 * Additions must be added here.
 *  
 */
public final class GeneticMapProperties {

    /**
     * This methods returns the integer code of the mapping
     * function associated to the given genetic map. 
     * 
     * @return the mapping function integer code.
     * @see IConsensusConstants, IGeneticMapProperties.
     */
    public static int getMappingFunction(IBioGenome map) {
    	
        String mapping = 
        	map.getProperties().getProperty(IBioGenomeProperties.MAPPING_FUNCTION);
        
        if (mapping != null) {
        	
	        if (mapping.equals(IBioGenomeProperties.MAPPING_FUNCTION_HALDANE))
	        	return IMetaQtlConstants.MAPPING_FUNCTION_HALDANE;
	        else if (mapping.equals(IBioGenomeProperties.MAPPING_FUNCTION_KOSAMBI))
	        	return IMetaQtlConstants.MAPPING_FUNCTION_KOSAMBI;
	        
        }
        /* By default return Haldane function */
        return IMetaQtlConstants.MAPPING_FUNCTION_HALDANE;
    }
    /**
     * This routine returns an object which handle the
     * properties of the offspring which have been used  
     * to create the genetic map. This object is created
     * from the cross properties of the genetic map.
     * 
     * @param map a genetic map
     * @return the cross population type for this genetic map
     * @see IConsensusConstants, IGeneticMapProperties.
     */
    public static CrossPopulation getCrossDesign(IBioGenome map) {
    	int size,type,gen;
    	String value;
    	
    	/* Get the size of the population */
    	value = map.getProperties().getProperty(IBioGenomeProperties.CROSS_SIZE);
    	if (value == null)
    		size = 0;
    	else
    		size = Integer.parseInt(value);
    	/* Get the type of the population */
    	value = map.getProperties().getProperty(IBioGenomeProperties.CROSS_TYPE);
    	if (value == null) {
    		type = IBioCrossTypes.UNKNOWN;
    		gen  = 0;
    	} else {
    		BioCrossTypes.parseCross(value);
    		type = BioCrossTypes.type;
    		gen  = BioCrossTypes.gen;
    	}
    	
        CrossPopulation cross = new CrossPopulation();
        cross.setCrossType(type);
        cross.setSize(size);
        cross.setGeneration(gen);
            
        return cross;
    }
	/**
	 * @param map
	 * @return
	 */
	public static int getMappingUnit(IBioGenome map) {
		String mapping = 
        	map.getProperties().getProperty(IBioGenomeProperties.MAPPING_UNIT);
        
		if (mapping != null) {
			if (mapping.equals(IBioGenomeProperties.MAPPING_UNIT_M))
	        	return IMetaQtlConstants.MAPPING_UNIT_M;
	        else if (mapping.equals(IBioGenomeProperties.MAPPING_UNIT_CM))
	        	return IMetaQtlConstants.MAPPING_UNIT_CM;
		}
	    /*
	     * By default return CM unit
	     */ 
		
        return IMetaQtlConstants.MAPPING_UNIT_CM;
	}
	/**
	 * Returns true if the given map need to be rescaled - due
	 * to the mapping extansion for some pedigree designs.
	 * @param map
	 * @return 
	 */
	public static boolean needToBeRescaled(IBioGenome map) {
		String mapping = 
        	map.getProperties().getProperty(IBioGenomeProperties.MAPPING_EXPANSION);
        
		if (mapping != null) {
			if (mapping.equals("1"))
	        	return true;
	        else return false;
		}
	    /*
	     * By default return false;
	     */ 
		return false;
	}
	/**
	 * @param map
	 * @param mapping_function_haldane
	 * @param mapping_unit_cm
	 */
	public static void setMappingContext(IBioGenome map, int mapping_function, int mapping_unit) {
		Properties prop = map.getProperties();
		if (prop==null) prop = new Properties();
		// function
		if (mapping_function == IMetaQtlConstants.MAPPING_FUNCTION_HALDANE)
			prop.setProperty(IBioGenomeProperties.MAPPING_FUNCTION, IBioGenomeProperties.MAPPING_FUNCTION_HALDANE);
		if (mapping_function == IMetaQtlConstants.MAPPING_FUNCTION_KOSAMBI)
			prop.setProperty(IBioGenomeProperties.MAPPING_FUNCTION, IBioGenomeProperties.MAPPING_FUNCTION_KOSAMBI);
		// unit
		if (mapping_unit == IMetaQtlConstants.MAPPING_UNIT_M)
			prop.setProperty(IBioGenomeProperties.MAPPING_UNIT, IBioGenomeProperties.MAPPING_UNIT_M);
		if (mapping_unit == IMetaQtlConstants.MAPPING_UNIT_CM)
			prop.setProperty(IBioGenomeProperties.MAPPING_UNIT, IBioGenomeProperties.MAPPING_UNIT_CM);		
	}
 	/**
	 * @param map
	 * @param crossDesign
	 */
	public static void setCrossDesign(IBioGenome map, CrossPopulation crossDesign) {
		Properties prop = map.getProperties();
		if (prop==null) prop = new Properties();
		// Size
		prop.setProperty(IBioGenomeProperties.CROSS_SIZE, Integer.toString(crossDesign.getSize()));
		// Type
		String code = BioCrossTypes.crossToString(crossDesign.getCrossType(), crossDesign.getGeneration());
		prop.setProperty(IBioGenomeProperties.CROSS_TYPE, code);
	}
	/**
	 * @param map
	 * @return
	 */
	public static CrossPopulation getMappingCross(IBioGenome map) {
		int type,gen;
		String value = map.getProperties().getProperty(IBioGenomeProperties.MAPPING_CROSS);
    	if (value == null) {
    		type = IBioCrossTypes.UNKNOWN;
    		gen  = 0;
    	} else {
    		BioCrossTypes.parseCross(value);
    		type = BioCrossTypes.type;
    		gen  = BioCrossTypes.gen;
    	}
    	
        CrossPopulation cross = new CrossPopulation();
        cross.setCrossType(type);
        cross.setGeneration(gen);
            
        return cross;
	}
	
}
