/*  
 *  src/org/metaqtl/util/LocusProperties.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:55 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl.util;

import org.metaqtl.IBioLocusProperties;
import org.metaqtl.bio.IBioLocus;

/**
 * @author jveyrier
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public final class LocusProperties {

	/**
	 * Returns true if the given locus has a property order.dubious
	 * set to one, false otherwise
	 * @param map
	 * @return 
	 */
	public static boolean isDubiousOrderLocus(IBioLocus locus) {
		String value = 
        	locus.getProperties().getProperty(IBioLocusProperties.DUBIOUS_ORDER);
        
		if (value != null) {
			if (value.equals("1"))
	        	return true;
	        else return false;
		}
	    /*
	     * By default return false;
	     */ 
		return false;
	}
	
	public static int getMetaOccurence(IBioLocus locus) {
		String value = 
        	locus.getProperties().getProperty(IBioLocusProperties.OCCURENCE);
        
		if (value != null) {
			return Integer.parseInt(value);
		} else return 0;	    
	}
	
}
