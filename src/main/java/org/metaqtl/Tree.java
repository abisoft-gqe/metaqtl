/*  
 *  src/org/metaqtl/Tree.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

import java.util.ArrayList;


/**
 * This class defines a tree.
 */
public class Tree  {
	/**
	 * The nodes of the tree.
	 */
	public TreeNode[] nodes=null;
	/**
	 * The root of the tree.
	 */
	public TreeNode root=null;
	
	private int nl = 0;
	
	private int nn = 0;
	
	
	public Tree() {}
	/**
	 * @param root
	 */
	public Tree(TreeNode root) {
		this.root  = root;
		this.nodes = initNodes(root, 0.0);
		this.nl    = getLeafNumber();
		this.nn    = nodes.length-nl;
	}
	/**
	 * 
	 * @param root
	 * @return
	 */
	private TreeNode[] initNodes(TreeNode root, double h) {
		TreeNode[] nodes=null;
		
		if (!root.leaf && root.children != null) {
			int nn=root.children.length;
			ArrayList nodeList = new ArrayList(root.children.length);
			for(int i=0;i<root.children.length;i++) {
				TreeNode[] t = initNodes(root.children[i], root.children[i].dist);
				if (t != null) {
					nn += t.length;
					nodeList.add(t);
				}
			}
			nodes     = new TreeNode[nn];
			root.card = nn;
			for(int i=0;i<root.children.length;i++) {
				nodes[i]         = root.children[i];
				nodes[i].height  = h+root.children[i].dist;
			}
			for(int j=root.children.length,i=0;i<nodeList.size();i++) {
				TreeNode[] t = (TreeNode[]) nodeList.get(i);
				for(int k=0;k<t.length;k++)	{
					t[k].height += h;
					nodes[j++]   = t[k];
				}
			}
		}
		return nodes;
	}
	/**
	 * @return
	 */
	public int getLeafNumber() {
		if (nodes==null) return 0;
		if (nl>0) return nl; 
		int nl=0;
		for(int i=0;i<nodes.length;i++)
			if (nodes[i].leaf) nl++; 
		return nl;
	}
	/**
	 * @return
	 */
	public int getNodeNumber() {
		if (nodes==null) return 0;
		return nn;
	}
	/**
	 * @return
	 */
	public double[] getLeafHeights() {
		if (nl==0) return null;
		double[] h = new double[nl];
		for(int j=0,i=0;i<nodes.length;i++) {
			if (nodes[i].leaf)
				h[j++]=nodes[i].height;
		}
		return h;
	}
	/**
	 * @return
	 */
	public double[] getNodeHeights() {
		if (nn==0) return null;
		double[] h = new double[nn];
		for(int j=0,i=0;i<nodes.length;i++) {
			if (!nodes[i].leaf)
				h[j++]=nodes[i].height;
		}
		return h;
	}
	/**
	 * @return
	 */
	public TreeNode getRoot() {
		return root;
	}
	/**
	 * @param i
	 * @return
	 */
	public TreeNode getLeaf(int i) {
		for(int j=0;j<nodes.length;j++)
			if (nodes[j].leaf && nodes[j].idx==i)
				return nodes[j];
		return null;
	}
	/**
	 * 
	 */
	public void initReverse() {
		recParent(root);
	}
	
	private void recParent(TreeNode node) {
		TreeNode[] children = node.getChildren();
		if (children != null) {
			for(int i=0;i<children.length;i++) {
				children[i].parent=node;
				recParent(children[i]);
			}
		}
	}
	
	public void initNodeIdx() {
		setNodeIdx(root);
	}
	
	private void setNodeIdx(TreeNode node) {
		if (node.leaf) {
			node.nidx = new int[1];
			node.nidx[0] = node.idx;
		} else {
			TreeNode[] children = node.getChildren();
			ArrayList nidxx     = new ArrayList();
			for(int i=0;i<children.length;i++) {
				setNodeIdx(children[i]);
				for(int j=0;j<children[i].nidx.length;j++) 
					nidxx.add(new Integer(children[i].nidx[j]));
			}
			node.nidx = new int[nidxx.size()];
			for(int i=0;i<node.nidx.length;i++) {
				node.nidx[i]=((Integer)nidxx.get(i)).intValue();
			}
		}
	}
	/**
	 * @param qtl_idx
	 */
	public void updateLeafIdx(int[] nidx) {
		for(int i=0;i<nodes.length;i++) {
			if (nodes[i].leaf) {
				nodes[i].idx = nidx[nodes[i].idx];
			}
		}
	}
}
