/*  
 *  src/org/metaqtl/IMetaQtlConstants.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;

/**
 * This interface defines some global constants which are used
 * by several classes of the package. 
 */
public interface IMetaQtlConstants {
	/**
	 * The version of the package.
	 */
	public static final String VERSION = "1.0";
	/**
	 * Integer code for not defined mapping function */
	public static final int MAPPING_FUNCTION_UNKNOWN = 0;
	/** Integer code for haldane mapping function */
	public static final int MAPPING_FUNCTION_HALDANE = 1;
	/** Integer code for kosambi mapping function */
	public static final int MAPPING_FUNCTION_KOSAMBI = 2;
	/** Integer code for unknown mapping unit */
	public static final int MAPPING_UNIT_UNKNOWN = 0;
	/** Integer code for morgan mapping unit */
	public static final int MAPPING_UNIT_M = 1;
	/** Integer code for centi morgan mapping unit */
	public static final int MAPPING_UNIT_CM = 2;
	/** The minimal distance in morgan */
	public static final double TINY_MAPPING_DISTANCE = 1.e-5;
	/** the minimal variance in morgan */
	public static final double TINY_MAPPING_VARIANCE = 1.e-32;
	/**
	 * SD Mode : use the available information to compute the 
	 * standard deviation.
	 */
	public static final int CI_MODE_AVAILABLE = 1;
	/**
	 * SD Mode : use only the confidence interval information
	 * if defined to compute the standard deviation. 
	 */
	public static final int CI_MODE_LOD 	  = 2;
	/**
	 * SD Mode : use only the rsquare information
	 * if defined to compute the standard deviation.
	 */
	public static final int CI_MODE_RSQUARE	  = 3;
	/**
	 * SD Mode : compute the standard deviation using
	 * both lod support and rsquare information (if available) 
	 * and then choose the maximal standard-deviation. 
	 */
	public static final int CI_MODE_MAXIMAL   = 4;
	
	/**
	 * MSD Mode : replace missing standard deviation
	 * by the mean of the observed ones.
	 */
	public static final int CI_MISS_IMPUTATION = 1;	
	/**
	 * MSD Mode : ignore QTL with missing standard deviation.
	 */
	public static final int CI_MISS_IGNORE     = 2;
	
}
