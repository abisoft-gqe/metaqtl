/*  
 *  src/org/metaqtl/MetaQtlModel.java
 * 
 *  $Author: veyrieras $, $Date: 2006-10-17 15:41:54 $, $Version$
 *
 *  MetaQTL : a Java package for QTL Meta-Analysis
 * 
 *  Copyright (C) 2003-2006  Jean-Baptiste Veyrieras, INRA, France.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA * 
 */
package org.metaqtl;


/**
 * 
 */
public class MetaQtlModel {
	/**
	 * The name of the criterion.
	 */
	private String criterion;
	/**
	 * The chromosomes.
	 */
	public String[] chromNames;
	/**
	 * The trait names per chromosome.
	 */
	public String[][] traitNames;
	/**
	 * The number of Qtl per chromosome
	 * and per trait.
	 */
	public int[][] models;
	
	
	public MetaQtlModel() {};
	
	public MetaQtlModel(int nchr) {
		
		chromNames = new String[nchr];
		traitNames = new String[nchr][];
		models     = new int[nchr][];
		
	}
	/**
	 * Returns the names of the chromosomes.
	 * 
	 * @param chrom
	 * @return
	 */
	public String[] getChromNames() {
		return chromNames;
	}
	/**
	 * @param chrom
	 * @return
	 */
	public String[] getTraitNames(String chrom) {
		if (chromNames==null) return null;
		for(int i=0;i<chromNames.length;i++) {
			if (chromNames[i].equals(chrom))
				return traitNames[i];
		}
		return null;
	}
	/**
	 * 
	 * @param chrom
	 * @return
	 */
	public String[] getTraitNames(int cidx) {
		if (chromNames==null) return null;
		return traitNames[cidx];
	}
	/**
	 * @param chrom
	 * @param string
	 * @return
	 */
	public int getModel(String chrom, String trait) {
		if (chromNames==null) return 0;
		for(int i=0;i<chromNames.length;i++) {
			if (chromNames[i].equals(chrom)) {
				for(int j=0;j<traitNames[i].length;j++) {
					if (traitNames[i][j].equals(trait))
						return models[i][j];
				}
			}
		}
		return 0;
	}
	
	/**
	 * 
	 * @param cidx
	 * @param tidx
	 */
	public void setModel(int cidx, int[] models) {
		if (models == null) return;
		this.models[cidx] = new int[models.length];
		for(int i=0;i<models.length;i++)
			this.models[cidx][i] = models[i];
	}
	/**
	 * 
	 * @param cidx
	 * @param name
	 */
	public void setChromName(int cidx, String name) {
		chromNames[cidx]=new String(name);
	}
	/**
	 * 
	 * @param cidx
	 * @param name
	 */
	public void setTraitNames(int cidx, String[] traits) {
		if (traits==null) return;
		traitNames[cidx]=new String[traits.length];
		for(int i=0;i<traits.length;i++)
			traitNames[cidx][i]=new String(traits[i]);
	}

	/**
	 * Set the criterion name which was used to obtain this model
	 * @param critName
	 */
	public void setCriterion(String critName) {
		this.criterion = new String(critName);
	}

	/**
	 * @param i
	 * @param j
	 * @return
	 */
	public int getModel(int i, int j) {
		return models[i][j];
	}

	/**
	 * @return
	 */
	public String getCriterion() {
		return criterion;
	}
}
